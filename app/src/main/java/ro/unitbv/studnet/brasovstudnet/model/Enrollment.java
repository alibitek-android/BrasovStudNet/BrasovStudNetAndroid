package ro.unitbv.studnet.brasovstudnet.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Enrollment implements Parcelable {
    private String nume;
    private String cicluDeInvatamant;
    private String formaDeInvatamant;
    private String domeniul;
    private String anDeStudiu;
    private String programulDeStudii;
    private Integer grupa;

    public Enrollment() {
    }

    public Enrollment(String nume, String cicluDeInvatamant, String formaDeInvatamant, String domeniul, String anDeStudiu, String programulDeStudii, Integer grupa) {
        this.nume = nume;
        this.cicluDeInvatamant = cicluDeInvatamant;
        this.formaDeInvatamant = formaDeInvatamant;
        this.domeniul = domeniul;
        this.anDeStudiu = anDeStudiu;
        this.programulDeStudii = programulDeStudii;
        this.grupa = grupa;
    }

    protected Enrollment(Parcel in) {
        nume = in.readString();
        cicluDeInvatamant = in.readString();
        formaDeInvatamant = in.readString();
        domeniul = in.readString();
        anDeStudiu = in.readString();
        programulDeStudii = in.readString();
        grupa = (Integer) in.readSerializable();
    }

    public static final Creator<Enrollment> CREATOR = new Creator<Enrollment>() {
        @Override
        public Enrollment createFromParcel(Parcel in) {
            return new Enrollment(in);
        }

        @Override
        public Enrollment[] newArray(int size) {
            return new Enrollment[size];
        }
    };

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getCicluDeInvatamant() {
        return cicluDeInvatamant;
    }

    public void setCicluDeInvatamant(String cicluDeInvatamant) {
        this.cicluDeInvatamant = cicluDeInvatamant;
    }

    public String getFormaDeInvatamant() {
        return formaDeInvatamant;
    }

    public void setFormaDeInvatamant(String formaDeInvatamant) {
        this.formaDeInvatamant = formaDeInvatamant;
    }

    public String getDomeniul() {
        return domeniul;
    }

    public void setDomeniul(String domeniul) {
        this.domeniul = domeniul;
    }

    public String getAnDeStudiu() {
        return anDeStudiu;
    }

    public void setAnDeStudiu(String anDeStudiu) {
        this.anDeStudiu = anDeStudiu;
    }

    public String getProgramulDeStudii() {
        return programulDeStudii;
    }

    public void setProgramulDeStudii(String programulDeStudii) {
        this.programulDeStudii = programulDeStudii;
    }

    public Integer getGrupa() {
        return grupa;
    }

    public void setGrupa(Integer grupa) {
        this.grupa = grupa;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nume);
        dest.writeString(cicluDeInvatamant);
        dest.writeString(formaDeInvatamant);
        dest.writeString(domeniul);
        dest.writeString(anDeStudiu);
        dest.writeString(programulDeStudii);
        dest.writeSerializable(grupa);
    }

    @Override
    public String toString() {
        return "Enrollment{" +
                "nume='" + nume + '\'' +
                ", cicluDeInvatamant='" + cicluDeInvatamant + '\'' +
                ", formaDeInvatamant='" + formaDeInvatamant + '\'' +
                ", domeniul='" + domeniul + '\'' +
                ", anDeStudiu='" + anDeStudiu + '\'' +
                ", programulDeStudii='" + programulDeStudii + '\'' +
                ", grupa=" + grupa +
                '}';
    }
}
