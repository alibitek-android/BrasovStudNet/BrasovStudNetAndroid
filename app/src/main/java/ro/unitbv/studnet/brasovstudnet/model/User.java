package ro.unitbv.studnet.brasovstudnet.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class User implements Comparable<User>, Parcelable {
    protected String id;
    protected String name;
    protected String email;
    protected String role;
    protected List<Topic> topics;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public User(String id, String name, String email, String role, List<Topic> topics) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.role = role;
        this.topics = topics;
    }

    public User(String name, String email, String role) {
        this.name = name;
        this.email = email;
        this.role = role;
    }

    public User(String name, String email, String role, List<Topic> topics) {
        this.name = name;
        this.email = email;
        this.role = role;
        this.topics = topics;
    }

    protected User(Parcel in) {
        id = in.readString();
        name = in.readString();
        email = in.readString();
        role = in.readString();
        topics = in.createTypedArrayList(Topic.CREATOR);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!name.equals(user.name)) return false;
        return email.equals(user.email);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }

    @Override
    public int compareTo(@NonNull User another) {
        return email.compareTo(another.getEmail());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeTypedList(topics);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public List<Topic> getTopics() {
        return topics;
    }

    public List<Topic> getTopicsByType(String type) {
        List<Topic> filteredTopics = new ArrayList<>();
        for (Topic topic : topics) {
            if (topic != null && topic.getType().equals(type)) {
                filteredTopics.add(topic);
            }
        }

        return filteredTopics;
    }

    public List<Topic> getTopicsByType(TopicType type) {
        return getTopicsByType(type.name());
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }


    public String getDetails() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                ", topics=" + topics +
                '}';
    }


}
