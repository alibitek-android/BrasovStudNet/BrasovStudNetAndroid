package ro.unitbv.studnet.brasovstudnet.ui;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.model.Group;
import ro.unitbv.studnet.brasovstudnet.model.User;


@EActivity(R.layout.activity_genericlist)
public class GenericMessageListActivity extends BaseActivity
        implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener, SearchView.OnCloseListener {

    private static final String TAG = GenericMessageListActivity.class.getSimpleName();

    @ViewById(R.id.genericList)
    ListView listView;

    @Extra
    String category;

    private DataCursorAdapter dataCursorAdapter;

    SearchView mSearchView;

    /*
    https://twitter.com/readystate/status/260666365705338880
    Stop using FilterQueryProvider. CursorLoader is also a replacement for that: restart it with your filter and you'll be fine.
     */

    // If non-null, this is the current filter the user has provided.
    String mCurFilter;

    @AfterViews
    void initializeViews() {
        String title = DataProvider.BroadcastMessageCategory.valueOf(category).name();
        if (title.equals("WhatsHot")) {
            title = "What's Hot";
        }

        Log.d(TAG, "Initializing views for: " + title);

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setSubtitle(app.getAccount());

        listView.setOnItemClickListener(this);
        dataCursorAdapter = new DataCursorAdapter(this, null);
        listView.setAdapter(dataCursorAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//
//		ArrayAdapter<CharSequence> dropdownAdapter = ArrayAdapter.createFromResource(this, R.array.dropdown_arr, android.R.layout.simple_list_item_1);
//		actionBar.setListNavigationCallbacks(dropdownAdapter, new ActionBar.OnNavigationListener() {
//
//			@Override
//			public boolean onNavigationItemSelected(int itemPosition, long itemId) {
//				getLoaderManager().restartLoader(0, getArgs(itemPosition), MainActivity.this);
//				return true;
//			}
//		});

        // Prepare the loader.  Either re-connect with an existing one, or start a new one.
        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //Log.d(TAG, String.valueOf(position) + " " + parent.getItemAtPosition(position));

        Cursor cursor = (Cursor) dataCursorAdapter.getItem(position);
        String message = cursor.getString(cursor.getColumnIndex(DataProvider.COL_MESSAGE));

//        Cursor messageCursor = getContentResolver()
//        .query(Uri.withAppendedPath(DataProvider.CONTENT_URI_MESSAGES, String.valueOf(messageId)), null, null, null, null);
        View messageContent = LayoutInflater.from(GenericMessageListActivity.this).inflate(R.layout.message_content, parent, false);
        TextView msgText = (TextView) messageContent.findViewById(R.id.messageText);
        msgText.setText(message.trim());
        msgText.setMovementMethod(new ScrollingMovementMethod());

        DialogPlus dialog = new DialogPlus.Builder(this)
                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(messageContent))
                .setCancelable(true)
                .setGravity(Gravity.CENTER)
                .create();

        dialog.show();

//        Cursor c = getContentResolver()
//                .query(Uri.withAppendedPath(DataProvider.CONTENT_URI_USERS, String.valueOf(id)),
//                        null, null, null, null);
//
//        if (c != null && c.moveToFirst()) {
//            User user = new User();
//            user.setName(c.getString(c.getColumnIndex(DataProvider.COL_NAME)));
//            user.setEmail(c.getString(c.getColumnIndex(DataProvider.COL_EMAIL)));
//            c.close();
//
//            ChatActivity.UserWrapper wrapper = new ChatActivity.UserWrapper(user);
//
//            Intent intent = new Intent(this, ChatActivity_.class);
//            intent.putExtra(Constants.USER, wrapper);
//            startActivity(intent);
//        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri baseUri;

        if (mCurFilter != null) {
            baseUri = Uri.withAppendedPath(DataProvider.CONTENT_URI_MESSAGES, Uri.encode(mCurFilter.toLowerCase()));
        } else {
            baseUri = DataProvider.CONTENT_URI_MESSAGES;
        }

        if (category.equals(DataProvider.BroadcastMessageCategory.WhatsHot.name())) {
            return new CursorLoader(this,
                    baseUri.buildUpon().appendQueryParameter(DataProvider.QUERY_PARAMETER_LIMIT, "10").build(),
                    new String[]{
                            DataProvider.COL_ID,
                            DataProvider.COL_TYPE,
                            DataProvider.COL_CATEGORY,
                            DataProvider.COL_SENDER_EMAIL,
                            DataProvider.COL_RECEIVER_EMAIL,
                            DataProvider.COL_MESSAGE,
                            DataProvider.COL_TIME
                    },
                    "category > 0",
                    null,
                    DataProvider.COL_TIME + " DESC");
        } else {
            return new CursorLoader(this,
                    baseUri,
                    new String[]{
                            DataProvider.COL_ID,
                            DataProvider.COL_TYPE,
                            DataProvider.COL_CATEGORY,
                            DataProvider.COL_SENDER_EMAIL,
                            DataProvider.COL_RECEIVER_EMAIL,
                            DataProvider.COL_MESSAGE,
                            DataProvider.COL_TIME
                    },
                    "category = ?",
                    new String[]{String.valueOf(DataProvider.BroadcastMessageCategory.valueOf(category).ordinal())},
                    DataProvider.COL_TIME + " DESC");
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // // Swap the new cursor in. (The framework will take care of closing the old cursor once we return.)
        //Log.d(TAG, "New Data: " + String.valueOf(data.getCount()));

        dataCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        dataCursorAdapter.swapCursor(null);
    }

    public class DataCursorAdapter extends CursorAdapter {

        private LayoutInflater mInflater;

        public DataCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
            this.mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return getCursor() == null ? 0 : super.getCount();
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View itemLayout = mInflater.inflate(R.layout.activity_genericlist_listitem, parent, false);

            MyViewHolder holder = new MyViewHolder();
            itemLayout.setTag(holder);

            holder.peerAvatar = (ImageView) itemLayout.findViewById(R.id.avatar);
            holder.peerUsername = (TextView) itemLayout.findViewById(R.id.peerUsername);
            holder.messagePreview = (TextView) itemLayout.findViewById(R.id.messagePreview);
//            holder.peerEmail = (TextView) itemLayout.findViewById(R.id.peerEmail);
            holder.receivedTime = (TextView) itemLayout.findViewById(R.id.receivedTime);

            return itemLayout;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            MyViewHolder holder = (MyViewHolder) view.getTag();

            String peerUsername = "";

            for (Group group : app.getGroups()) {
                for (User user : group.getUsers()) {
                    if (user.getEmail().equals(cursor.getString(cursor.getColumnIndex(DataProvider.COL_SENDER_EMAIL)))) {
                        peerUsername = user.getName();
                        break;
                    }
                }
            }

            Calendar calendar = new GregorianCalendar();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss", Locale.getDefault());

            java.util.Date dt = null;

            try {
                dt = sdf.parse(cursor.getString(cursor.getColumnIndex(DataProvider.COL_TIME)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            calendar.setTime(dt);

            // FIXME: For some reason sometimes the time in the UI is shown 1 year in advance but inside the database is correct
            holder.receivedTime.setText(sdf.format(calendar.getTime()));

            holder.peerUsername.setText(peerUsername);

//            holder.peerEmail.setText(cursor.getString(cursor.getColumnIndex(DataProvider.COL_SENDER_EMAIL)));

            String message = cursor.getString(cursor.getColumnIndex(DataProvider.COL_MESSAGE));
            holder.messagePreview.setText(message.length() > 50 ? message.substring(0, 50) : message);

            int category = cursor.getInt(cursor.getColumnIndex(DataProvider.COL_CATEGORY));
            holder.peerAvatar.setImageResource(app.getMessageCategoryIcon(DataProvider.BroadcastMessageCategory.values()[category].name()));

        }
    }

    private static class MyViewHolder {
        TextView peerUsername;
        TextView messagePreview;
        TextView peerEmail;
        ImageView peerAvatar;
        TextView receivedTime;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        if (searchView != null) {
            mSearchView = searchView;

            // Associate searchable configuration with the SearchView
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String s) {

                    // Called when the action bar search text has changed.  Update
                    // the search filter, and restart the loader to do a new query
                    // with this filter.
                    String newFilter = !TextUtils.isEmpty(s) ? s.toLowerCase() : null;

                    // Don't do anything if the filter hasn't actually changed.
                    // Prevents restarting the loader when restoring state.
                    if (mCurFilter == null && newFilter == null) {
                        return true;
                    }

                    if (mCurFilter != null && mCurFilter.equals(newFilter)) {
                        return true;
                    }

                    mCurFilter = newFilter;
                    getSupportLoaderManager().restartLoader(0, null, GenericMessageListActivity.this);

                    return true;
                }
            });
        } else {
            Log.w(TAG, "searchView is null");
        }

        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }
}