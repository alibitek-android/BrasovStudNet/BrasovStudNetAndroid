package ro.unitbv.studnet.brasovstudnet.service;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

@EBean(scope = EBean.Scope.Singleton)
public class RestClientProxy {
    @RestService
    RestClientService instance;

    @Bean
    RestClientErrorHandler errorHandler;

    @AfterInject
    void init() {
        for (HttpMessageConverter<?> myConverter : instance.getRestTemplate().getMessageConverters ()) {
            if (myConverter instanceof MappingJackson2HttpMessageConverter) {
                List<MediaType> myMediaTypes = new ArrayList<MediaType>();
                myMediaTypes.addAll (myConverter.getSupportedMediaTypes ());
                myMediaTypes.add(MediaType.parseMediaType ("application/json;charset=UTF-8"));
                ((MappingJackson2HttpMessageConverter) myConverter).setSupportedMediaTypes (myMediaTypes);
            }
        }

        instance.setRestErrorHandler(errorHandler);
    }

    public RestClientService getInstance() {
        return instance;
    }
}
