package ro.unitbv.studnet.brasovstudnet.ui;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp_;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.model.Topic;
import ro.unitbv.studnet.brasovstudnet.service.GcmManager;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EActivity(R.layout.activity_broadcastchat)
public class BroadcastMessagingActivity extends BaseActivity {

    private static final String TAG = "BroadcastMessaging";

    private String from;
    private String to;

    public boolean isGroup(String name) {
        return name.contains("@");
    }

    private boolean sendToGroup;

//    @ViewById(R.id.drawer_layout)
//    DrawerLayout mDrawerLayout;
//
//    @ViewById(R.id.top_toolbar)
//    Toolbar toolbar;

    @ViewById(R.id.userTextView)
    TextView userTv;

    @ViewById(R.id.txtMessage)
    TextView messageTv;

//    @ViewById(R.id.nav_view)
//    NavigationView navigationView;

    @ViewById
    Spinner categorySpinner;

    @Bean
    GcmManager gcmManager;

    String spinnerSelectedItem;
//
//    @App
//    BrasovStudNetApp app;

    // TODO: Implement chatactivity as a viewpager with a list of messages
    // X ChatMessagingActivity for one-to-one chat messaging
    // GroupMessagingActivity for many-to-many chat messaging
    // X BroadcastMessagingActivity for one-to-many messaging

    @AfterViews
    void initializeViews() {
        // Toolbar
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setTitle(getString(R.string.title_activity_broadcast));
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //toolbar.inflateMenu(R.menu.menu_top);

//        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_48dp);
//        ab.setDisplayHomeAsUpEnabled(true);

        // NavigationView
//        if (navigationView != null) {
//            setupDrawerContent(navigationView);
//        }

        from = getIntent().getStringExtra(Constants.CHAT_FROM);
        to = getIntent().getStringExtra(Constants.CHAT_TO);

        userTv.setText(to);
        toolbar.setSubtitle(to);

        // TODO: Store message in local database
        // (message, from, to, itime)
        //final TextView chatHistoryTv = (TextView) findViewById(R.id.chatHistory);

        Button sendMessageButton = (Button) findViewById(R.id.btnSendMessage);
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new SendMessageAsyncTask(null, messageTv).execute(messageTv.getText().toString(), null, null);
                String address = null;
                for (Topic topic : app.getUser().getTopics()) {
                    if (topic.getName().equals(to)) {
                        address = topic.getAddress();
                        break;
                    }
                }

                if (address != null) {
                    sendMessage(messageTv.getText().toString(), address, spinnerSelectedItem);
                } else {
                    Log.e(TAG, "Cannot find address of the topic for group: " + to);
                }
            }
        });

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.broadcast_categories_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        categorySpinner.setAdapter(adapter);

        categorySpinner.setSelection(0);
    }

    @ItemSelect(R.id.categorySpinner)
    public void spinnerItemSelected(boolean selected, String selectedItem) {
        Log.d(TAG, "Selected spinner item: " + selectedItem);
        spinnerSelectedItem = selectedItem;
    }


    public String getBroadcastMessageCategory() {
        return spinnerSelectedItem;
    }
//
//    public int getBroadcastMessageCategory() {
//        return DataProvider.BroadcastMessageCategory.valueOf(spinnerSelectedItem).ordinal();
//    }

    @Background
    void sendMessage(final String message, final String address, String messageCategory) {
        boolean status = gcmManager.sendMessage(message, address);

//        ContentValues values = new ContentValues(4);
//        values.put(DataProvider.COL_TYPE, DataProvider.MessageType.BROADCAST.ordinal());
//        values.put(DataProvider.COL_CATEGORY, DataProvider.BroadcastMessageCategory.valueOf(getBroadcastMessageCategory()).ordinal());
//        values.put(DataProvider.COL_MESSAGE, message);
//        values.put(DataProvider.COL_RECEIVER_EMAIL, address);
//        values.put(DataProvider.COL_SENDER_EMAIL, app.getAccount());
//        getContentResolver().insert(DataProvider.CONTENT_URI_MESSAGES, values);

        updateUi(status);
    }

    @UiThread
    void updateUi(final boolean status) {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageTv.setText("");

                if (status) {
                    Toast.makeText(BroadcastMessagingActivity.this, "Successfully sent broadcast message", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(BroadcastMessagingActivity.this, "An error occurred while sending the boardcast message", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        // Toolbar
//        toolbar.setNavigationIcon(R.drawable.ic_menu_white_48dp);
//        toolbar.setTitle(getString(R.string.app_name));
//        setSupportActionBar(toolbar);
//        toolbar.inflateMenu(R.menu.menu_top);

//        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_48dp);
//        ab.setDisplayHomeAsUpEnabled(true);
    }

    private final class SendMessageAsyncTask extends AsyncTask<String, Void, String> {
        TextView chatHistoryTextView;
        TextView messageTextView;

        public SendMessageAsyncTask(TextView chatHistory, TextView messageText) {
            this.chatHistoryTextView = chatHistory;
            this.messageTextView = messageText;
        }

        @Override
        protected String doInBackground(String... params) {
            String msg = params[0];

            try {
                Bundle data = new Bundle();
                Log.d(TAG, "Trying to send message: " + msg);
                data.putString(Constants.KEY_MESSAGE_TXT, msg);
                data.putString(Constants.ACTION, Constants.ACTION_ECHO);

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                int msgID = prefs.getInt(Constants.KEY_MSG_ID, 0);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(Constants.KEY_MSG_ID, ++msgID);
                editor.apply();

                String id = Integer.toString(msgID);
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                gcm.send(getString(R.string.gcm_defaultSenderId) + "@gcm.googleapis.com", id, data);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                Log.e(TAG, msg);
            }

            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            if (chatHistoryTextView != null)
                chatHistoryTextView.append(msg + "\n");
            messageTextView.setText("");
        }
    }
}
