package ro.unitbv.studnet.brasovstudnet.ui;

import android.content.Context;
import android.util.AttributeSet;

import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;
import xyz.danoz.recyclerviewfastscroller.sectionindicator.title.SectionTitleIndicator;


/**
 * Indicator for sections of type {@link ro.unitbv.studnet.brasovstudnet.model.User}
 */
public class PeopleGroupSectionTitleIndicator extends SectionTitleIndicator<PeopleGroupSectionTitleIndicator.SectionWrapper> {

    static class SectionWrapper {
        public String type;
        public String selectedType;
        public Object ptr;

        public SectionWrapper(String type, Object ptr) {
            this.type = type;
            this.ptr = ptr;
        }

        public SectionWrapper(String type, String selectedType, Object ptr) {
            this.type = type;
            this.selectedType = selectedType;
            this.ptr = ptr;
        }

        @Override
        public String toString() {
            return String.valueOf(ptr);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SectionWrapper that = (SectionWrapper) o;

            if (type != null ? !type.equals(that.type) : that.type != null) return false;
            if (selectedType != null ? !selectedType.equals(that.selectedType) : that.selectedType != null)
                return false;
            return !(ptr != null ? !ptr.equals(that.ptr) : that.ptr != null);

        }

        @Override
        public int hashCode() {
            int result = type != null ? type.hashCode() : 0;
            result = 31 * result + (selectedType != null ? selectedType.hashCode() : 0);
            result = 31 * result + (ptr != null ? ptr.hashCode() : 0);
            return result;
        }
    }

    public PeopleGroupSectionTitleIndicator(Context context) {
        super(context);
    }

    public PeopleGroupSectionTitleIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PeopleGroupSectionTitleIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSection(SectionWrapper section) {

        // groups
        if (section.selectedType == null) {
            setTitleText(String.valueOf(section.ptr));
        } else { // people
            // from campus group
            if (section.selectedType.equals(Constants.STUDENT_GROUP_CAMPUS)) {
                setTitleText(String.valueOf(section.ptr));
            } else if (section.selectedType.equals(Constants.STUDENT_GROUP_SCHOOL)) { // from school group
                setTitleText(String.valueOf(section.ptr));
            }
        }

        setIndicatorTextColor(R.color.material_deeporangeA200);
    }

}