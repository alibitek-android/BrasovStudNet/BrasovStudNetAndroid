package ro.unitbv.studnet.brasovstudnet.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Student extends User implements Parcelable {
    private Enrollment enrollment;
    private Accomodation accomodation;
    private Location location;

    protected Student(Parcel in) {
        super(in);
        enrollment = in.readParcelable(getClass().getClassLoader());
        accomodation = in.readParcelable(getClass().getClassLoader());
        location = in.readParcelable(getClass().getClassLoader());
    }

    public Student(String name, String email) {
        super(name, email);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(enrollment, flags);
        dest.writeParcelable(accomodation, flags);
        dest.writeParcelable(location, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Student() {
        super();
    }

    public Student(String id, String name, String email, String role) {
        super(id, name, email, role, null);
    }

    public Student(String id, String name, String email, String role, List<Topic> topics) {
        super(id, name, email, role, topics);
    }


    public Student(String id, String name, String email, String role, List<Topic> topics, Enrollment enrollment, Accomodation accomodation) {
        super(id, name, email, role, topics);
        this.enrollment = enrollment;
        this.accomodation = accomodation;
    }

    public Student(String name) {
        super(name);
    }

    public Student(String name, String email, String role) {
        super(name, email, role, null);
    }

    public Student(String name, String email, String role, List<Topic> topics) {
        super(name, email, role, topics);
    }

    public Student(String name, String email, String role, List<Topic> topics, Enrollment enrollment, Accomodation accomodation) {
        super(name, email, role, topics);
        this.enrollment = enrollment;
        this.accomodation = accomodation;
    }

    public Student(String name, String email, String role, List<Topic> topics, Enrollment enrollment, Accomodation accomodation, Location location) {
        super(name, email, role, topics);
        this.enrollment = enrollment;
        this.accomodation = accomodation;
        this.location = location;
    }

    public Enrollment getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
    }

    public Accomodation getAccomodation() {
        return accomodation;
    }

    public void setAccomodation(Accomodation campus) {
        this.accomodation = campus;
    }

    public String getDetails() {
        return "Student{" +
                "enrollment=" + enrollment +
                ", accomodation=" + accomodation +
                ", location=" + location +
                '}';
    }
}
