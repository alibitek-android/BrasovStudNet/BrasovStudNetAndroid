package ro.unitbv.studnet.brasovstudnet.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.ui.ChatActivity;
import ro.unitbv.studnet.brasovstudnet.ui.ChatActivity_;
import ro.unitbv.studnet.brasovstudnet.ui.GenericMessageListActivity_;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

// enables various aspects of handling messages such as detecting different downstream message types,
// determining upstream send status, and automatically displaying simple notifications on the app’s behalf.
public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "Received: " + data.toString());

        String action = data.getString("action", "");

        switch (action) {
            case Constants.ACTION_PEER_TOKEN_NOT_REGISTERED:
//                //sendNotification("User " + data.getString("peer") + " has not registered yet!");
//                Log.w(TAG, "User " + data.getString("peer") + " has not registered yet!");
//                break;

            case Constants.ACTION_CHAT_MESSAGE:
            case Constants.ACTION_BROADCAST_MESSAGE:
                String message = data.getString("message");
                String senderEmail = data.getString("fromAcc");
                String receiverEmail = data.getString("toAcc");

                Log.d(TAG, "From: " + senderEmail);
                Log.d(TAG, "To: " + receiverEmail);
                Log.d(TAG, "Message: " + message);

                if (senderEmail != null) {
//                    if (action.equals(Constants.ACTION_BROADCAST_MESSAGE) && !senderEmail.equals(((BrasovStudNetApp) (getApplication())).getAccount()))) {
                        ContentValues values = new ContentValues(4);

                        String category = null;

                        if (action.equals(Constants.ACTION_CHAT_MESSAGE)) {
                            values.put(DataProvider.COL_TYPE,  DataProvider.MessageType.INCOMING.ordinal());
                        } else if (action.equals(Constants.ACTION_BROADCAST_MESSAGE)) {
                            values.put(DataProvider.COL_TYPE,  DataProvider.MessageType.BROADCAST.ordinal());
                            values.put(DataProvider.COL_CATEGORY, DataProvider.BroadcastMessageCategory.valueOf(data.getString(Constants.MESSAGE_CATEGORY)).ordinal());
                            category = data.getString(Constants.MESSAGE_CATEGORY);
                        }

                        values.put(DataProvider.COL_MESSAGE, message);
                        values.put(DataProvider.COL_SENDER_EMAIL, senderEmail);
                        values.put(DataProvider.COL_RECEIVER_EMAIL, receiverEmail);
                        getContentResolver().insert(DataProvider.CONTENT_URI_MESSAGES, values);

                        sendNotification(message, receiverEmail, action, category);

//                    }
                }

                break;

            default:
                Log.w(TAG, "No action! From: " + from + " Data: " + data);
                break;
        }

        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        // TODO: Store the message in the local database

    }

    @Override
    public void onDeletedMessages() {
        Log.d(TAG, "Deleted messages on server");
    }

    @Override
    public void onMessageSent(String msgId) {
        Log.d(TAG, "Upstream message sent. Id=" + msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        Log.d(TAG, "Upstream message send error. Id=" + msgId + ", error" + error);
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *http://stackoverflow.com/questions/30704840/android-transfering-data-via-contentintent
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message, String to, String action, String category) {

        Intent intent = new Intent();

        if (category != null) {
            intent.setClass(this, GenericMessageListActivity_.class);
            intent.putExtra(Constants.MESSAGE_CATEGORY, category);
        } else {
            intent.setClass(this, ChatActivity_.class);
            ChatActivity.UserWrapper ur = new ChatActivity.UserWrapper(to, to);
            intent.putExtra(Constants.USER, ur);
        }


        // Bring the chat activity to the top of the stack (and remove all the other activities)
        // and don't restart it if already started
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String title = "Brașov StudNet - ";

        switch (action) {
            case Constants.ACTION_CHAT_MESSAGE:
                title += "Chat Message";
                break;

            case Constants.ACTION_BROADCAST_MESSAGE:
                title += category;
                break;

            default:
                title += "Message";
                break;
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(((BrasovStudNetApp) (getApplication())).getMessageCategoryIcon(category))
                .setContentTitle(title) // + ((message.length() > 50) ? message.substring(0, 50) + "..." : message)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
