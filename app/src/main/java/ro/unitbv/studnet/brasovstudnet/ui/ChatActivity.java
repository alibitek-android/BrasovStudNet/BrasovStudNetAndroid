package ro.unitbv.studnet.brasovstudnet.ui;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.service.GcmManager;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EActivity(R.layout.activity_chat)
public class ChatActivity extends BaseActivity
        implements MessagesFragment.OnFragmentInteractionListener, View.OnClickListener {

    @ViewById(R.id.msg_edit)
    EditText msgEdit;

    @ViewById(R.id.send_btn)
    Button sendBtn;

//    @App
//    BrasovStudNetApp app;

    @Extra
    UserWrapper user;

    @Bean
    GcmManager gcmManager;

//    @ViewById(R.id.top_toolbar)
//    Toolbar toolbar;
//
//    @ViewById(R.id.drawer_layout)
//    DrawerLayout mDrawerLayout;

    public static class UserWrapper implements Parcelable {
        String name;
        String email;

        public UserWrapper(String name, String email) {
            this.name = name;
            this.email = email;
        }

        public UserWrapper(User user) {
            this.name = user.getName();
            this.email = user.getEmail();
        }

        protected UserWrapper(Parcel in) {
            name = in.readString();
            email = in.readString();
        }

        public static final Creator<UserWrapper> CREATOR = new Creator<UserWrapper>() {
            @Override
            public UserWrapper createFromParcel(Parcel in) {
                return new UserWrapper(in);
            }

            @Override
            public UserWrapper[] newArray(int size) {
                return new UserWrapper[size];
            }
        };

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(email);
        }
    }

    @AfterViews
    void afterViews() {
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setTitle(getString(R.string.title_activity_chat));
        toolbar.setSubtitle(user.getName());

//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sendBtn.setOnClickListener(this);
    }

    @AfterExtras
    void afterExtras() {
        Cursor c = getContentResolver()
                .query(Uri.withAppendedPath(DataProvider.CONTENT_URI_USERS, user.getEmail()),
                        null, null, new String[]{user.getEmail()}, null);

        if (c == null || c.getCount() == 0) {
            ContentValues values = new ContentValues(3);
            values.put(DataProvider.COL_NAME, user.getName());
            values.put(DataProvider.COL_EMAIL, user.getEmail());
            values.put(DataProvider.COL_COUNT, 0);
            getContentResolver().insert(DataProvider.CONTENT_URI_USERS, values);
        }

        if (c != null) {
            c.close();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver(registrationStatusReceiver, new IntentFilter(BrasovStudNetApp.ACTION_REGISTER));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top, menu);
        menu.findItem(R.id.action_search).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_btn: {
                sendMessage(msgEdit.getText().toString());
                msgEdit.setText(null);
                break;
            }
        }
    }

    @Override
    public String getPeerEmail() {
        return user.getEmail();
    }

    @Background
    void sendMessage(final String message) {
        boolean status = gcmManager.sendMessage(message, getPeerEmail());

        ContentValues values = new ContentValues(4);
        values.put(DataProvider.COL_TYPE, DataProvider.MessageType.OUTGOING.ordinal());
        values.put(DataProvider.COL_MESSAGE, message);
        values.put(DataProvider.COL_RECEIVER_EMAIL, user.getEmail());
        values.put(DataProvider.COL_SENDER_EMAIL, app.getAccount());
        getContentResolver().insert(DataProvider.CONTENT_URI_MESSAGES, values);

        showSendMessageStatus(status);
    }

    @UiThread // for some reason this doesn't work
    void showSendMessageStatus(final boolean status) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                if (status) {
                    Toast.makeText(ChatActivity.this, "Successfully sent message", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(ChatActivity.this, "An error occurred while sending the message", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        ContentValues values = new ContentValues(1);
        values.put(DataProvider.COL_COUNT, 0);
        getContentResolver().update(Uri.withAppendedPath(DataProvider.CONTENT_URI_USERS, user.getEmail()), values, null, null);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(registrationStatusReceiver);
        gcmManager.cleanup();
        super.onDestroy();
    }

    private BroadcastReceiver registrationStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && BrasovStudNetApp.ACTION_REGISTER.equals(intent.getAction())) {
                ActionBar actionBar = getSupportActionBar();

                if (actionBar != null) {
                    switch (intent.getIntExtra(BrasovStudNetApp.EXTRA_STATUS, 100)) {
                        case BrasovStudNetApp.STATUS_SUCCESS:
                            actionBar.setSubtitle("online");
                            sendBtn.setEnabled(true);
                            break;

                        case BrasovStudNetApp.STATUS_FAILED:
                            actionBar.setSubtitle("offline");
                            break;
                    }
                }
            }
        }
    };

}