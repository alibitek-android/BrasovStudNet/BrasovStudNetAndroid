package ro.unitbv.studnet.brasovstudnet.ui;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp_;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EActivity
public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @ViewById(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById(R.id.top_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.navigationHeaderUsername)
    TextView headerUsername;

    @ViewById(R.id.navigationHeaderEmail)
    TextView headerEmail;

    @App
    BrasovStudNetApp app;

    @AfterViews
    void initializeBaseViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_menu_white_48dp);
        toolbar.setTitle(R.string.app_name);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        User user = ((BrasovStudNetApp_) getApplication()).getUser();
        if (user != null) {
//            Log.d(TAG, "User: " + user.getName() + " Email: " + user.getEmail());
//            Log.d(TAG, user.getTopics().toString());
            headerUsername.setText(user.getName());
            headerEmail.setText(user.getEmail());
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        mDrawerLayout.closeDrawers();

        int id = menuItem.getItemId();


        Intent intent = new Intent(this, GenericMessageListActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        switch (id) {
            case R.id.action_whatshot:
                intent.putExtra(Constants.MESSAGE_CATEGORY, DataProvider.BroadcastMessageCategory.WhatsHot.name());
                break;

            case R.id.action_announcement:
                intent.putExtra(Constants.MESSAGE_CATEGORY, DataProvider.BroadcastMessageCategory.Announcements.name());
                break;

            case R.id.action_assessments:
                intent.putExtra(Constants.MESSAGE_CATEGORY, DataProvider.BroadcastMessageCategory.Assessments.name());
                break;

            case R.id.action_assignements:
                intent.putExtra(Constants.MESSAGE_CATEGORY, DataProvider.BroadcastMessageCategory.Assignments.name());
                break;

            case R.id.action_events:
                intent.putExtra(Constants.MESSAGE_CATEGORY, DataProvider.BroadcastMessageCategory.Events.name());
                break;

            case R.id.actions_business:
                intent.putExtra(Constants.MESSAGE_CATEGORY, DataProvider.BroadcastMessageCategory.Business.name());
                break;

            case R.id.action_poll:
                intent.putExtra(Constants.MESSAGE_CATEGORY, DataProvider.BroadcastMessageCategory.Polls.name());
                break;

            case R.id.action_schedule:
                intent.setClass(this, ScheduleActivity_.class);
                break;

            case R.id.action_people:
                intent.setClass(this, MainActivity_.class);
                intent.putExtra(Constants.KEY_ACCOUNT, ((BrasovStudNetApp_) getApplication()).getAccount());
                break;

            case R.id.action_messages:
                intent.setClass(this, MessagesActivity_.class);
                break;

            default:
                Toast.makeText(this, "[Navigation View] You clicked on an unhandled menu item! Menu Item ID: " + String.valueOf(id)
                                + " Menu Title: " + menuItem.getTitle(),
                        Toast.LENGTH_LONG).show();
                break;
        }

        startActivity(intent);

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id)
        {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_search:
                break;

            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;

            case R.id.action_feedback:
                startActivity(new Intent(this, FeedbackActivity_.class));
                break;

            case R.id.action_about:
                startActivity(new Intent(this, AboutActivity_.class));
                break;

            case R.id.action_signout:
                PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
                startActivity(new Intent(this, LoginActivity_.class));
                break;

            default:
                Toast.makeText(this, "[Top Toolbar] You clicked on an unhandled menu item! Menu Item ID: " + String.valueOf(id) + " Menu Title: " + item.getTitle(), Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
