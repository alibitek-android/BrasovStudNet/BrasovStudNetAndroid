package ro.unitbv.studnet.brasovstudnet.ui;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.Favorite;

@EActivity(R.layout.activity_favorites)
public class FavoritesActivity extends ListActivity {

    @App
    BrasovStudNetApp application;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        //setListAdapter(new ArrayAdapter<Favorite>(this, android.R.layout.simple_list_item_1, application.favorites));
        registerForContextMenu(getListView());
    }

    @ItemClick
    void listItemClicked(Favorite fav) {
//        Intent i = null;
//        if (fav.getObj() instanceof Stop) {
//            Stop stop = (Stop) fav.getObj();
//            Toast.makeText(this, "click: " + stop.getStop_name(),
//                    Toast.LENGTH_SHORT).show();
//            i = new Intent(this, StopsDetailsActivity_.class);
//            i.putExtra("stop", (Parcelable) stop);
//        } else if (fav.getObj() instanceof Route) {
//            Route route = (Route) fav.getObj();
//            Toast.makeText(this, "click: " + route.getRoute_long_name(),
//                    Toast.LENGTH_SHORT).show();
//            i = new Intent(this, RouteDetailsActivity_.class);
//            i.putExtra("route", route);
//        }
//
//        startActivity(i);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Favorites Context Menu");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.favorites_context_menu, menu);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean onContextItemSelected(MenuItem aItem) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) aItem
                .getMenuInfo();

        switch (aItem.getItemId()) {
            case R.id.favorite_delete:
                Favorite favContexted = (Favorite) getListView().getAdapter()
                        .getItem(menuInfo.position);

                //application.favorites.remove(favContexted);

                ArrayAdapter<Favorite> aa = (ArrayAdapter<Favorite>) getListAdapter();
                aa.notifyDataSetChanged();
                return true;
        }
        return false;
    }

    public boolean saveArray(String[] array, String arrayName, Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("favorites", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(arrayName + "_size", array.length);
        for (int i = 0; i < array.length; i++)
            editor.putString(arrayName + "_" + i, array[i]);
        return editor.commit();
    }

    public String[] loadArray(String arrayName, Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("favorites", 0);
        int size = prefs.getInt(arrayName + "_size", 0);
        String array[] = new String[size];
        for (int i = 0; i < size; i++)
            array[i] = prefs.getString(arrayName + "_" + i, null);
        return array;
    }
}
