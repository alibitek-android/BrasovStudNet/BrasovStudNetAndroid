package ro.unitbv.studnet.brasovstudnet.service;

import android.util.Log;

import org.androidannotations.annotations.EBean;
import org.androidannotations.api.rest.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;

@EBean
public class RestClientErrorHandler implements RestErrorHandler {
    private static final String TAG = RestClientErrorHandler.class.getSimpleName();

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {
        Log.e(TAG, e.getMessage());
    }
}
