package ro.unitbv.studnet.brasovstudnet.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;

public class Topic implements Parcelable {
    private String name;
    private String address;
    private String type;

    protected Topic(Parcel in) {
        name = in.readString();
        address = in.readString();
        type = in.readString();
//        int itype = in.readInt();

//        if (itype >= 0 && itype <= TopicType.values().length - 1)
//            type = TopicType.values()[in.readInt()];
    }

    public static final Creator<Topic> CREATOR = new Creator<Topic>() {
        @Override
        public Topic createFromParcel(Parcel in) {
            return new Topic(in);
        }

        @Override
        public Topic[] newArray(int size) {
            return new Topic[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(type);
//        dest.writeInt(type.ordinal());
    }

//    class StringDeSerializer extends JsonDeserializer<TopicType> {
//
//        @Override
//        public TopicType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
//            return Topic.TopicType.valueOf(p.getValueAsString());
//        }
//    }

//    @JsonIgnoreType
//    @JsonFormat(shape = JsonFormat.Shape.STRING)
////    @JsonDeserialize(using = StringDeSerializer.class)
//

    public Topic () {}

//
//    public Topic(String name, String address, TopicType type) {
//        this.name = name;
//        this.address = address;
//        this.type = type;
//    }

    public Topic(String name, String address, String type) {
        this.name = name;
        this.address = address;
        setType(type);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
//
//    public TopicType getType() {
//        return type;
//    }
//
//    public void setType(TopicType type) {
//        this.type = type;
//    }
//    public void setType(String type) {
//        this.type = TopicType.valueOf(type.toUpperCase());
//    }


    @Override
    public String toString() {
        return "Topic{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
