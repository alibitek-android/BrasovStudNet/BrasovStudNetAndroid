package ro.unitbv.studnet.brasovstudnet.utils;


public interface Constants {

    String KEY_STATE = "keyState";
    String KEY_REG_ID = "keyRegId";
    String KEY_MSG_ID = "keyMsgId";

    String KEY_ACCOUNT = "account";
    String KEY_PASSWORD = "password";

    String KEY_MESSAGE_TXT = "message";
    String KEY_EVENT_TYPE = "keyEventbusType";

    String ACTION = "action";

    long GCM_DEFAULT_TTL = 15;

    String PACKAGE = "ro.unitbv.studnet.brasovstudnet.gcm";

    // server interaction
    String ACTION_REGISTER = PACKAGE + ".REGISTER";
    String ACTION_UNREGISTER = PACKAGE + ".UNREGISTER";
    String ACTION_ECHO = PACKAGE + ".ECHO";

    String ACTION_CHAT_MESSAGE = PACKAGE + ".CHAT_MESSAGE";
    String ACTION_BROADCAST_MESSAGE = PACKAGE + ".BROADCAST_MESSAGE";
    String ACTION_GROUP_MESSAGE = PACKAGE + ".GROUP_MESSAGE";

    String ACTION_TOKEN_REFRESH = PACKAGE + ".TOKENREFRESH";
    String ACTION_PEER_TOKEN_NOT_REGISTERED = PACKAGE + ".PEER_TOKEN_NOT_REGISTERED";

    // notification intent
    String NOTIFICATION_ACTION = PACKAGE + ".NOTIFICATION";

    String DEFAULT_USER = "fakeUser";
    String MESSAGE_CATEGORY = "category";

    enum MessageType {
        REGISTRATION_FAILED, REGISTRATION_SUCCEEDED, UNREGISTRATION_SUCCEEDED, UNREGISTRATION_FAILED;
    }

    enum State {
        REGISTERED, UNREGISTERED;
    }

    String GROUP_ITEM = "group_item";
    String GROUP_TYPE = "group_type";
    String GROUP = "group";
    String USER = "user";

    String STUDENT_GROUP_SCHOOL = "STUDENT_GROUP_SCHOOL";
    String STUDENT_GROUP_CAMPUS = "STUDENT_GROUP_CAMPUS";
    String FAVORITES_GROUP = "FAVORITES_GROUP";
    String TEACHER_GROUP = "TEACHER_GROUP";
    String PEOPLE = "PEOPLE";
    String CHAT_FROM = "CHAT_FROM";
    String CHAT_TO = "CHAT_TO";

    String TOPICS = "topics";
    String GROUP_ADDRESS = "groupAddress";

    String EXTRA_STATUS = "status";
    int STATUS_SUCCESS = 1;
    int STATUS_FAILED = 0;
}