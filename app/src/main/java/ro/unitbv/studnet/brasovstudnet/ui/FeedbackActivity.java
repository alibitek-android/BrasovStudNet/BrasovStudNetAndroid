package ro.unitbv.studnet.brasovstudnet.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import ro.unitbv.studnet.brasovstudnet.R;

@EActivity(R.layout.activity_feedback)
public class FeedbackActivity extends AppCompatActivity {

    Button submitButton;
    EditText commentsField;
    private static final int EMAIL_REQUEST_CODE = 0;

    public void enableSubmitIfReady() {
        boolean isReady = commentsField.getText().toString().length() > 3;
        submitButton.setEnabled(isReady);
    }

    @AfterViews
    void afterViews() {
        commentsField = (EditText) findViewById(R.id.commentsField);
        submitButton = (Button) findViewById(R.id.submitButton);

        commentsField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                enableSubmitIfReady();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        wireButtons();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void wireButtons() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback();
            }
        });
    }

    protected void sendFeedback() {
        AlertDialog.Builder confirm = new AlertDialog.Builder(this);

        if (commentsField.getText().length() == 0) {
            confirm.setTitle("Feedback Not Sent!");
            confirm.setMessage("Please fill out the form before submitting.");
            confirm.setNegativeButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        } else if (!sendFeedbackEmail(commentsField.getText().toString())) {
            confirm.setTitle("Feedback Failed!");
            confirm.setMessage("Unable to send feedback at this time.");
            confirm.setNegativeButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            FeedbackActivity.this.finish();
                        }
                    });
        }

        confirm.show();
    }

    public boolean sendFeedbackEmail(String body)
    {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"alex.butum@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Brasov Student - Feedback");
        i.putExtra(Intent.EXTRA_TEXT, body);
        try {
            startActivityForResult(Intent.createChooser(i, "Send feedback mail"), EMAIL_REQUEST_CODE);
            return true;
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(FeedbackActivity.this, "There are no peerEmail clients installed.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    protected void onActivityResult(
            int aRequestCode, int aResultCode, Intent aData) {

        AlertDialog.Builder confirm = new AlertDialog.Builder(this);

        if (aRequestCode == EMAIL_REQUEST_CODE) {
            confirm.setTitle("Feedback Success!");
            confirm.setMessage("Thank you for your feedback.");
            confirm.setNegativeButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            FeedbackActivity.this.finish();
                        }
                    });

            confirm.show();
        } else if (aResultCode == RESULT_CANCELED) {
            confirm.setTitle("Feedback Canceled!");
            confirm.setMessage("Are you sure you wanted to do this? If not, please retry sending the feedback!");
            confirm.setNegativeButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            FeedbackActivity.this.finish();
                        }
                    });

            confirm.show();
        }

        super.onActivityResult(aRequestCode, aResultCode, aData);
    }
}