package ro.unitbv.studnet.brasovstudnet.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Accomodation implements Parcelable {
    private String name;
    private Integer campusNumber;
    private Integer roomNumber;
    private String address;
    private Integer constructionYear;
    private Integer areaSqm;
    private Integer numberOfPlaces;
    private String administrator;
    private String comitteChairman;
    private String phoneNumber;
    private String complex;



    public Accomodation(Parcel in) {
        name = in.readString();
        campusNumber = in.readInt();
        roomNumber = in.readInt();
        address = in.readString();
        constructionYear = in.readInt();
        areaSqm = in.readInt();
        numberOfPlaces = in.readInt();
        administrator = in.readString();
        comitteChairman = in.readString();
        phoneNumber = in.readString();
        complex = in.readString();
    }

    public static final Creator<Accomodation> CREATOR = new Creator<Accomodation>() {
        @Override
        public Accomodation createFromParcel(Parcel in) {
            return new Accomodation(in);
        }

        @Override
        public Accomodation[] newArray(int size) {
            return new Accomodation[size];
        }
    };

    public String getComplex() {
        return complex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComplex(String complex) {
        this.complex = complex;
    }

    public Accomodation() {

    }

    public Accomodation(Integer campusNumber, Integer roomNumber) {
        this.campusNumber = campusNumber;
        this.roomNumber = roomNumber;
    }

    public Accomodation(Integer campusNumber, Integer roomNumber, String address, Integer constructionYear, Integer areaSqm, Integer numberOfPlaces, String administrator, String comitteChairman, String phoneNumber) {
        this.campusNumber = campusNumber;
        this.roomNumber = roomNumber;
        this.address = address;
        this.constructionYear = constructionYear;
        this.areaSqm = areaSqm;
        this.numberOfPlaces = numberOfPlaces;
        this.administrator = administrator;
        this.comitteChairman = comitteChairman;
        this.phoneNumber = phoneNumber;
    }

    public Integer getCampusNumber() {

        return campusNumber;
    }

    public void setCampusNumber(Integer campusNumber) {
        this.campusNumber = campusNumber;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getConstructionYear() {
        return constructionYear;
    }

    public void setConstructionYear(Integer constructionYear) {
        this.constructionYear = constructionYear;
    }

    public Integer getAreaSqm() {
        return areaSqm;
    }

    public void setAreaSqm(Integer areaSqm) {
        this.areaSqm = areaSqm;
    }

    public Integer getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(Integer numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public String getAdministrator() {
        return administrator;
    }

    public void setAdministrator(String administrator) {
        this.administrator = administrator;
    }

    public String getComitteChairman() {
        return comitteChairman;
    }

    public void setComitteChairman(String comitteChairman) {
        this.comitteChairman = comitteChairman;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(campusNumber);
        dest.writeInt(roomNumber);
        dest.writeString(address);
        dest.writeInt(constructionYear);
        dest.writeInt(areaSqm);
        dest.writeInt(numberOfPlaces);
        dest.writeString(administrator);
        dest.writeString(comitteChairman);
        dest.writeString(phoneNumber);
        dest.writeString(complex);
    }

    @Override
    public String toString() {
        return "Accomodation{" +
                "name='" + name + '\'' +
                ", campusNumber=" + campusNumber +
                ", roomNumber=" + roomNumber +
                ", address='" + address + '\'' +
                ", constructionYear=" + constructionYear +
                ", areaSqm=" + areaSqm +
                ", numberOfPlaces=" + numberOfPlaces +
                ", administrator='" + administrator + '\'' +
                ", comitteChairman='" + comitteChairman + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", complex='" + complex + '\'' +
                '}';
    }
}
