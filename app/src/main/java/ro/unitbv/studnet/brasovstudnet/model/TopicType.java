package ro.unitbv.studnet.brasovstudnet.model;

public enum TopicType {
    SCHOOL,
    CAMPUS,
    BUSINESS;

    TopicType() {
    }

    @Override
    public String toString() {
        return name();
    }
}