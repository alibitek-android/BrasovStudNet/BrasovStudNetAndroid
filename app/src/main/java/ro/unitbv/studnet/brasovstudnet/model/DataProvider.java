package ro.unitbv.studnet.brasovstudnet.model;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * adb root
 * adb shell ls -l /data/data/ro.unitbv.studnet.brasovstudnet/databases
 * adb shell
 * sqlite3 /data/data/ro.unitbv.studnet.brasovstudnet/databases/brasovstudent.db
 */
public class DataProvider extends ContentProvider {

    private static final String TAG = "DataProvider";

    public static final Uri CONTENT_URI_MESSAGES = Uri.parse("content://ro.unitbv.studnet.brasovstudnet.provider/messages");
    public static final Uri CONTENT_URI_USERS = Uri.parse("content://ro.unitbv.studnet.brasovstudnet.provider/users");

    public static final String COL_ID = "_id";

    public enum MessageType {
        INCOMING, OUTGOING, BROADCAST
    }

    public enum BroadcastMessageCategory {
        Default, Announcements, Events, Assignments, Assessments, Business, Polls, WhatsHot
    }

    public static final String QUERY_PARAMETER_LIMIT = "limit";
    public static final String QUERY_PARAMETER_OFFSET = "offset";

    // TABLE MESSAGES
    public static final String TABLE_MESSAGES 		= "messages";
    public static final String COL_TYPE				= "type";
    public static final String COL_CATEGORY			= "category";
    public static final String COL_SENDER_EMAIL 	= "senderEmail";
    public static final String COL_RECEIVER_EMAIL 	= "receiverEmail";
    public static final String COL_MESSAGE 			= "message";
    public static final String COL_TIME 			= "time";

    // TABLE USERS
    public static final String TABLE_USERS = "users";
    public static final String COL_NAME = "name";
    public static final String COL_EMAIL = "email";
    public static final String COL_COUNT = "message_count";

    private DbHelper dbHelper;

    private static final int MESSAGES_ALLROWS = 1;
    private static final int MESSAGES_SINGLE_ROW = 2;
    private static final int MESSAGES_MULTIPLE_ROWS = 3;

    private static final int USERS_ALLROWS = 4;
    private static final int USERS_SINGLE_ROW = 5;
    private static final int USERS_EMAIL_ROW = 6;

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("ro.unitbv.studnet.brasovstudnet.provider", "messages", MESSAGES_ALLROWS);
        uriMatcher.addURI("ro.unitbv.studnet.brasovstudnet.provider", "messages/#", MESSAGES_SINGLE_ROW);
        uriMatcher.addURI("ro.unitbv.studnet.brasovstudnet.provider", "messages/*", MESSAGES_MULTIPLE_ROWS);
        uriMatcher.addURI("ro.unitbv.studnet.brasovstudnet.provider", "users", USERS_ALLROWS);
        uriMatcher.addURI("ro.unitbv.studnet.brasovstudnet.provider", "users/#", USERS_SINGLE_ROW);
        uriMatcher.addURI("ro.unitbv.studnet.brasovstudnet.provider", "users/*", USERS_EMAIL_ROW);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String limit = uri.getQueryParameter(QUERY_PARAMETER_LIMIT);
        String offset = uri.getQueryParameter(QUERY_PARAMETER_OFFSET);

        String limitString = null;
        if (limit != null && offset != null) {
            limitString = offset + "," + limit;
        } else if (limit != null) {
            limitString = limit;
        } else if (offset != null) {
            limitString = offset + ", -1";
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        List<String> args = new ArrayList<>();

        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALLROWS:
                qb.setTables(TABLE_MESSAGES);
                break;

            case MESSAGES_SINGLE_ROW:
                qb.setTables(TABLE_MESSAGES);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;

            case MESSAGES_MULTIPLE_ROWS:
                qb.setTables(TABLE_MESSAGES);
                qb.appendWhere("message like ");
                qb.appendWhereEscapeString("%" + uri.getLastPathSegment() + "%");
                qb.appendWhere(" OR senderEmail like ");
                qb.appendWhereEscapeString("%" + uri.getLastPathSegment() + "%");
                break;

            case USERS_ALLROWS:
                qb.setTables(TABLE_USERS);
                break;

            case USERS_SINGLE_ROW:
                qb.setTables(TABLE_USERS);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;

            case USERS_EMAIL_ROW:
                qb.setTables(TABLE_USERS);
                qb.appendWhere("email = ?");
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        if (selectionArgs != null) {
            args.addAll(Arrays.asList(selectionArgs));
        }

        Cursor c = qb.query(db, projection, selection, args.toArray(new String[args.size()]), null, null, sortOrder, limitString);

        if (uriMatcher.match(uri) == MESSAGES_MULTIPLE_ROWS) {
            Log.d(TAG, String.valueOf(c.getCount()));

            while (c.moveToNext()) {
                Log.d(TAG, c.getString(c.getColumnIndex(COL_MESSAGE)));
            }

        }

        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        long id;
        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALLROWS:
                id = db.insertOrThrow(TABLE_MESSAGES, null, values);
                if (values.get(COL_RECEIVER_EMAIL) == null) {
                    db.execSQL("update profile set count = count+1 where email = ?", new Object[]{values.get(COL_SENDER_EMAIL)});
                    getContext().getContentResolver().notifyChange(CONTENT_URI_USERS, null);
                }
                break;

            case USERS_ALLROWS:
                id = db.insertOrThrow(TABLE_USERS, null, values);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        Uri insertUri = ContentUris.withAppendedId(uri, id);
        getContext().getContentResolver().notifyChange(insertUri, null);
        return insertUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int count;
        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALLROWS:
                count = db.update(TABLE_MESSAGES, values, selection, selectionArgs);
                break;

            case MESSAGES_SINGLE_ROW:
                count = db.update(TABLE_MESSAGES, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case USERS_ALLROWS:
                count = db.update(TABLE_USERS, values, selection, selectionArgs);
                break;

            case USERS_SINGLE_ROW:
                count = db.update(TABLE_USERS, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case USERS_EMAIL_ROW:
                count = db.update(TABLE_USERS, values, "email = ?", new String[]{uri.getLastPathSegment()});
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int count;

        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALLROWS:
                count = db.delete(TABLE_MESSAGES, selection, selectionArgs);
                break;

            case MESSAGES_SINGLE_ROW:
                count = db.delete(TABLE_MESSAGES, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case USERS_ALLROWS:
                count = db.delete(TABLE_USERS, selection, selectionArgs);
                break;

            case USERS_SINGLE_ROW:
                count = db.delete(TABLE_USERS, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case USERS_EMAIL_ROW:
                count = db.delete(TABLE_USERS, "email = ?", new String[]{uri.getLastPathSegment()});
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    private static class DbHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "brasovstudent.db";
        private static final int DATABASE_VERSION = 2;

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, new SQLiteCursorFactory(true), DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table messages ("
                    + "_id integer primary key autoincrement, "
                    + COL_TYPE        	  +" integer, "
                    + COL_CATEGORY        	  +" integer default 0, "
                    + COL_MESSAGE         +" text, "
                    + COL_SENDER_EMAIL 	  +" text, "
                    + COL_RECEIVER_EMAIL  +" text, "
                    + COL_TIME 			  +" datetime default current_timestamp);");

            db.execSQL("create table users("
                    + "_id integer primary key autoincrement, "
                    + COL_NAME 	  +" text, "
                    + COL_EMAIL   +" text unique, "
                    + COL_COUNT   +" integer default 0);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
            onCreate(db);
        }
    }

    /**
     * Cursor factory used to log the queries before returning the cursor
     */
    static class SQLiteCursorFactory implements SQLiteDatabase.CursorFactory {

        private boolean debugQueries = false;

        public SQLiteCursorFactory() {
            this.debugQueries = false;
        }

        public SQLiteCursorFactory(boolean debugQueries) {
            this.debugQueries = debugQueries;
        }

        @Override
        public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver masterQuery,
                                String editTable, SQLiteQuery query) {
            if (debugQueries) {
                Log.d("SQL", query.toString());
            }
            return new SQLiteCursor(masterQuery, editTable, query);
        }
    }
}