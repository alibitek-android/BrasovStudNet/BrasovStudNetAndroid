package ro.unitbv.studnet.brasovstudnet.ui;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EActivity(R.layout.activity_people)
public class PeopleActivity extends BaseActivity {

    private static final String TAG = "PeopleActivity";

//    @App
//    BrasovStudNetApp app;
//
//    @ViewById(R.id.drawer_layout)
//    DrawerLayout mDrawerLayout;
//
//    @ViewById(R.id.top_toolbar)
//    Toolbar toolbar;

    @ViewById(R.id.viewpager)
    ViewPager viewPager;

    @ViewById(R.id.fab)
    FloatingActionButton fab;

    @Extra
    GroupWrapper group;

    ListFragment peopleFragment;

    static class GroupWrapper implements Parcelable {
        String type;
        String name;
        String address;

        public GroupWrapper(String type, String name) {
            this.type = type;
            this.name = name;
        }

        public GroupWrapper(String type, String name, String address) {
            this.type = type;
            this.name = name;
            this.address = address;
        }

        protected GroupWrapper(Parcel in) {
            type = in.readString();
            name = in.readString();
            address = in.readString();
        }

        public static final Creator<GroupWrapper> CREATOR = new Creator<GroupWrapper>() {
            @Override
            public GroupWrapper createFromParcel(Parcel in) {
                return new GroupWrapper(in);
            }

            @Override
            public GroupWrapper[] newArray(int size) {
                return new GroupWrapper[size];
            }
        };

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(type);
            dest.writeString(name);
            dest.writeString(address);
        }

        @Override
        public String toString() {
            return "GroupWrapper{" +
                    "type='" + type + '\'' +
                    ", name='" + name + '\'' +
                    ", address='" + address + '\'' +
                    '}';
        }
    }

    @AfterViews
    void initializeViews() {
        Log.d(TAG, group.toString());

        // Toolbar
//        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setTitle(getString(R.string.app_name) + " " + group.getType());
        toolbar.setSubtitle(group.getName());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //toolbar.inflateMenu(R.menu.menu_top);

//        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_48dp);
//        ab.setDisplayHomeAsUpEnabled(true);

//        // NavigationView
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        if (navigationView != null) {
//            setupDrawerContent(navigationView);
//        }

        // ViewPager
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BroadcastMessagingActivity_.class);
                String from = app.getAccount();
                String to = group.getName();

                intent.putExtra(Constants.CHAT_FROM, from);
                intent.putExtra(Constants.CHAT_TO, to);
                Log.d(TAG, "Message from: " + from + " TO: " + to);
                startActivity(intent);

                Snackbar.make(view, "Send a message to a group of people", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());

        peopleFragment = new ListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.GROUP_TYPE, Constants.PEOPLE);
        bundle.putParcelable(Constants.GROUP, group);
        peopleFragment.setArguments(bundle);

        adapter.addFragment(peopleFragment, group.getName());

        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        if (searchView != null) {
            // Associate searchable configuration with the SearchView
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
//                    ArrayAdapter<User> adapter = new ArrayAdapter<User>(PeopleActivity.this, android.R.layout.simple_list_item_1, peopleFragment.users);
//                    adapter.getFilter().filter(s.toLowerCase());
                    peopleFragment.viewAdapter.getFilter().filter(s.toLowerCase());
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String s) {
//                    ArrayAdapter<User> adapter = new ArrayAdapter<User>(PeopleActivity.this, android.R.layout.simple_list_item_1, peopleFragment.users);
                    peopleFragment.viewAdapter.getFilter().filter(s.toLowerCase());
                    return true;
                }
            });
        } else {
            Log.w(TAG, "searchView is null");
        }

        return true;
    }
}
