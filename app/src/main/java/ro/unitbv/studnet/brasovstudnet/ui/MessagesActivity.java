package ro.unitbv.studnet.brasovstudnet.ui;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EActivity(R.layout.activity_messages)
public class MessagesActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

//    @ViewById(R.id.drawer_layout)
//    DrawerLayout mDrawerLayout;
//
//    @ViewById(R.id.top_toolbar)
//    Toolbar toolbar;

    @ViewById(R.id.contactslist)
    ListView listView;

    private ContactCursorAdapter contactCursorAdapter;

    @AfterViews
    void initializeViews() {
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_48dp);
        toolbar.setTitle(getString(R.string.title_activity_messages));
        toolbar.setSubtitle(app.getAccount());
//        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        if (navigationView != null) {
//            setupDrawerContent(navigationView);
//        }

        listView.setOnItemClickListener(this);
        contactCursorAdapter = new ContactCursorAdapter(this, null);
        listView.setAdapter(contactCursorAdapter);
    }

//    private void setupDrawerContent(NavigationView navigationView) {
//
//        navigationView.setNavigationItemSelectedListener(
//                new NavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                    public boolean onNavigationItemSelected(MenuItem menuItem) {
//                        menuItem.setChecked(true);
//                        mDrawerLayout.closeDrawers();
//
//                        int id = menuItem.getItemId();
//
//                        switch (id) {
//                            case R.id.action_people:
//                                startActivity(new Intent(getApplicationContext(), MainActivity_.class).putExtra(Constants.KEY_ACCOUNT, app.getAccount()).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                                break;
//
//                            case R.id.action_messages:
//                                startActivity(new Intent(getApplicationContext(), MessagesActivity_.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                                break;
//
//                            default:
//                                Toast.makeText(getApplicationContext(), "[Navigation View] You clicked on an unhandled menu item! Menu Item ID: " + String.valueOf(id)
//                                                + " Menu Title: " + menuItem.getTitle(),
//                                        Toast.LENGTH_LONG).show();
//                                break;
//                        }
//
//                        return true;
//                    }
//                });
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//
//		ArrayAdapter<CharSequence> dropdownAdapter = ArrayAdapter.createFromResource(this, R.array.dropdown_arr, android.R.layout.simple_list_item_1);
//		actionBar.setListNavigationCallbacks(dropdownAdapter, new ActionBar.OnNavigationListener() {
//
//			@Override
//			public boolean onNavigationItemSelected(int itemPosition, long itemId) {
//				getLoaderManager().restartLoader(0, getArgs(itemPosition), MainActivity.this);
//				return true;
//			}
//		});

        getSupportLoaderManager().initLoader(0, null, this);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_top, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
////            case R.id.action_add:
////                AddContactDialog newFragment = AddContactDialog.newInstance();
////                newFragment.show(getSupportFragmentManager(), "AddContactDialog");
////                return true;
//            case R.id.action_settings:
//                Intent intent = new Intent(this, SettingsActivity.class);
//                startActivity(intent);
//                return true;
//
//            case android.R.id.home:
//                mDrawerLayout.openDrawer(Gravity.START);
//                break;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    /**
     * Open a ChatActivity when an item is clicked on the MessagesActivity
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor c = getContentResolver()
                .query(Uri.withAppendedPath(DataProvider.CONTENT_URI_USERS, String.valueOf(id)),
                        null, null, null, null);

        if (c != null && c.moveToFirst()) {
            User user = new User();
            user.setName(c.getString(c.getColumnIndex(DataProvider.COL_NAME)));
            user.setEmail(c.getString(c.getColumnIndex(DataProvider.COL_EMAIL)));
            c.close();

            ChatActivity.UserWrapper wrapper = new ChatActivity.UserWrapper(user);

            Intent intent = new Intent(this, ChatActivity_.class);
            intent.putExtra(Constants.USER, wrapper);
            startActivity(intent);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,
                DataProvider.CONTENT_URI_USERS,
                new String[]{DataProvider.COL_ID, DataProvider.COL_NAME, DataProvider.COL_EMAIL, DataProvider.COL_COUNT},
                null,
                null,
                DataProvider.COL_ID + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursor, Cursor data) {
        contactCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        contactCursorAdapter.swapCursor(null);
    }

    public class ContactCursorAdapter extends CursorAdapter {

        private LayoutInflater mInflater;

        public ContactCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
            this.mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return getCursor() == null ? 0 : super.getCount();
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View itemLayout = mInflater.inflate(R.layout.activity_messages_listitem, parent, false);

            ViewHolder holder = new ViewHolder();
            itemLayout.setTag(holder);

            holder.peerUsername = (TextView) itemLayout.findViewById(R.id.peerUsername);
            holder.numberOfMessages = (TextView) itemLayout.findViewById(R.id.numberOfMessages);
            holder.peerEmail = (TextView) itemLayout.findViewById(R.id.textEmail);
            holder.peerAvatar = (ImageView) itemLayout.findViewById(R.id.avatar);

            return itemLayout;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ViewHolder holder = (ViewHolder) view.getTag();

            holder.peerUsername.setText(cursor.getString(cursor.getColumnIndex(DataProvider.COL_NAME)));
            holder.peerEmail.setText(cursor.getString(cursor.getColumnIndex(DataProvider.COL_EMAIL)));

            int count = cursor.getInt(cursor.getColumnIndex(DataProvider.COL_COUNT));

            if (count > 0) {
                holder.numberOfMessages.setVisibility(View.VISIBLE);
                holder.numberOfMessages.setText(String.format("%d new message%s", count, count == 1 ? "" : "s"));
            } else {
                holder.numberOfMessages.setVisibility(View.GONE);
            }

            holder.peerAvatar.setImageResource(R.drawable.ic_person_black_48dp);
        }
    }

    private static class ViewHolder {
        TextView peerUsername;
        TextView numberOfMessages;
        TextView peerEmail;
        ImageView peerAvatar;
    }
}
