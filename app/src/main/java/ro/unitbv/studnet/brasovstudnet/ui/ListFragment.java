package ro.unitbv.studnet.brasovstudnet.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp_;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.Group;
import ro.unitbv.studnet.brasovstudnet.model.Role;
import ro.unitbv.studnet.brasovstudnet.model.Student;
import ro.unitbv.studnet.brasovstudnet.model.Topic;
import ro.unitbv.studnet.brasovstudnet.model.TopicType;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.service.RestClientProxy;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;
import xyz.danoz.recyclerviewfastscroller.sectionindicator.title.SectionTitleIndicator;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

@EFragment
public class ListFragment extends Fragment {

    private static final String TAG = ListFragment.class.getSimpleName();

    String groupType;

    Topic[] topics;

    @Bean
    RestClientProxy restClient;

    RecyclerView recyclerView;
    RecyclerViewAdapter viewAdapter;
    VerticalRecyclerViewFastScroller fastScroller;

    List<Group> groups = new ArrayList<>();
    List<User> users = new ArrayList<>();

    PeopleActivity.GroupWrapper selectedGroup;

    BrasovStudNetApp_ app;

    List<PeopleGroupSectionTitleIndicator.SectionWrapper> sectionGroups = new LinkedList<>();

    private final Object mLock = new Object();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (BrasovStudNetApp_) getActivity().getApplication();

        if (groups.size() == 0 && app.getGroups().size() > 0) {
            groups.addAll(app.getGroups());
        }

        Bundle args = getArguments();

        groupType = args.getString(Constants.GROUP_TYPE);

        Log.d(TAG, "Group Type: " + groupType);

        if (Objects.equals(groupType, Constants.PEOPLE)) {
            selectedGroup = args.getParcelable(Constants.GROUP);

            Log.d(TAG, selectedGroup.toString());

            //viewAdapter = new SimpleRecyclerViewAdapter(getActivity(), (User[]) users.toArray(new User[users.size()]), groupType);
            viewAdapter = new RecyclerViewAdapter(getActivity());

            Log.d(TAG, "Selected group address: " + selectedGroup.getAddress() + " Groups size: " + app.getGroups().size());

            for (Group group : app.getGroups()) {
                if (group.getAddress().equals(selectedGroup.getAddress()) && group.getType().equals(selectedGroup.getType())) {
                    users.clear();
                    users.addAll(group.getUsers());

                    if (groupType.equals(Constants.STUDENT_GROUP_SCHOOL)) {
                        Collections.sort(users, new Comparator<User>() {

                            @Override
                            public int compare(User lhs, User rhs) {
                                return lhs.getName().substring(0, lhs.getName().indexOf(' ')).compareTo(rhs.getName().substring(0, lhs.getName().indexOf(' ')));
                            }
                        });
                    }

                    computeSections();
                    viewAdapter.notifyDataSetChanged();
                    break;
                }
            }
        } else {
//            if (args.containsKey(Constants.TOPICS)) {
            if (groupType.equals(Constants.STUDENT_GROUP_CAMPUS)) {
                List<Topic> topicsByType1 = app.getUser().getTopicsByType(
                        TopicType.CAMPUS);

                topics = (Topic[]) topicsByType1.toArray(new Topic[topicsByType1.size()]);
            } else if (groupType.equals(Constants.STUDENT_GROUP_SCHOOL)) {
                List<Topic> topicsByType2 = app.getUser().getTopicsByType(TopicType.SCHOOL);
                topics = (Topic[]) topicsByType2.toArray(new Topic[topicsByType2.size()]);
            }

//            topics = (Topic[]) args.getParcelableArray(Constants.TOPICS);

            Log.d(TAG, "Group Type: " + groupType + " Topics: " + topics.length);
//            }

            //viewAdapter = new SimpleRecyclerViewAdapter(getActivity(), (Group[]) groups.toArray(new Group[groups.size()]), groupType);
            viewAdapter = new RecyclerViewAdapter(getActivity());

            getGroupData(groupType, topics);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        ViewGroup rootView;

        if (Objects.equals(groupType, Constants.PEOPLE)) {
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list_with_section_indicator, container, false);

            fastScroller = (VerticalRecyclerViewFastScroller) rootView.findViewById(R.id.fast_scroller);
            SectionTitleIndicator sectionTitleIndicator = (SectionTitleIndicator) rootView.findViewById(R.id.fast_scroller_section_title_indicator);

            recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

            // Connect the recycler to the scroller (to let the scroller scroll the list)
            fastScroller.setRecyclerView(recyclerView);

            // Connect the scroller to the recycler (to let the recycler scroll the scroller's handle)
            recyclerView.addOnScrollListener(fastScroller.getOnScrollListener());

            // Connect the section indicator to the scroller
            fastScroller.setSectionIndicator(sectionTitleIndicator);

            setRecyclerViewLayoutManager(recyclerView);
        } else {
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list, container, false);
            recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        }

        recyclerView.setAdapter(viewAdapter);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (fastScroller != null)
            recyclerView.removeOnScrollListener(fastScroller.getOnScrollListener());
    }

    public void setRecyclerViewLayoutManager(RecyclerView recyclerView) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.getLayoutManager() != null) {
            scrollPosition =
                    ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.scrollToPosition(scrollPosition);
    }

//    @AfterViews
//    void afterViews() {
//        Log.d(TAG, "afterViews");
//        getGroupData(groupType, topics);
//    }

    @Background
    void getGroupData(String viewType, Topic[] topics) {

        if (topics == null) {
            Log.d(TAG, "null topics in getGroupData for: " + viewType);
            return;
        }

        switch (viewType) {
            case Constants.STUDENT_GROUP_CAMPUS:
            case Constants.STUDENT_GROUP_SCHOOL:

                List<Group> groups = new ArrayList<>();

                for (Topic topic : topics) {

                    List<Student> students = restClient.getInstance().getStudentsForTopic(topic.getAddress());

                    if (groupType.equals(Constants.STUDENT_GROUP_SCHOOL)) {
                        Collections.sort(students, new Comparator<Student>() {

                            @Override
                            public int compare(Student lhs, Student rhs) {
                                return lhs.getName().substring(0, lhs.getName().indexOf(' ')).compareTo(rhs.getName().substring(0, lhs.getName().indexOf(' ')));
                            }
                        });
                    }

                    groups.add(new Group(topic.getName(), viewType, topic.getAddress(), (Student[]) students.toArray(new Student[students.size()])));

                }

                //showGroup(DummyPeopleData.campusGroups);
                showGroup((Group[]) groups.toArray(new Group[groups.size()]));
                break;

//            case Constants.STUDENT_GROUP_SCHOOL:
//                assert topics != null;
//                showGroup(DummyPeopleData.schoolGroups);
//                break;

//            case Constants.PEOPLE:
//                showUsers(selectedGroup);
//                break;

            default:
                break;
        }
    }

    private void computeSections() {
        if (selectedGroup != null) { // TODO: Add scroller to groups also
            sectionGroups.clear();

            switch (groupType) {
                // First letter of the group represents the section
                case Constants.STUDENT_GROUP_SCHOOL:
                case Constants.STUDENT_GROUP_CAMPUS:

                    for (Group group : groups) {
                        sectionGroups.add(new PeopleGroupSectionTitleIndicator.SectionWrapper(groupType, group.getName().substring(0, 1)));
                    }

                case Constants.PEOPLE:
                    switch (selectedGroup.getType()) {
                        // First letter of the group represents the section
                        case Constants.STUDENT_GROUP_SCHOOL:
                            for (User user : users) {
                                sectionGroups.add(new PeopleGroupSectionTitleIndicator.SectionWrapper(groupType, selectedGroup.getType(),
                                        user.getName().substring(0, 1)));
                            }
                            break;
                        // The room number represents the section
                        case Constants.STUDENT_GROUP_CAMPUS:
                            for (User user : users) {
                                if (user.getRole().equals(Role.STUDENT.name())) {
                                    Student student = (Student) user;
                                    sectionGroups.add(new PeopleGroupSectionTitleIndicator.SectionWrapper(groupType, selectedGroup.getType(), student.getAccomodation().getRoomNumber()));
                                }
                            }
                            break;
                    }

                default:
                    break;
            }
        }

    }

    @UiThread
    void showGroup(Group[] data) {
//        System.arraycopy(data, 0, groups, 0, data.length);
        Log.d(TAG, "Showing group data for: " + groupType);
        Collections.addAll(groups, data);
        app.addGroups(groups);
        computeSections();
        viewAdapter.notifyDataSetChanged();
        //recyclerView.setAdapter(new SimpleRecyclerViewAdapter(getActivity(), data != null ? data : new Group[]{}, groupType));
    }

//    @UiThread
//    void showUsers(User[] data) {
//        Collections.addAll(users, data);
//
//        if (groupType.equals(Constants.STUDENT_GROUP_SCHOOL)) {
//            Collections.sort(users, new Comparator<User>() {
//
//                @Override
//                public int compare(User lhs, User rhs) {
//                    return lhs.getName().substring(0, lhs.getName().indexOf(' ')).compareTo(rhs.getName().substring(0, lhs.getName().indexOf(' ')));
//                }
//            });
//        }
//
//        computeSections();
//        viewAdapter.notifyDataSetChanged();
//
//        //recyclerView.setAdapter(new SimpleRecyclerViewAdapter(getActivity(), data != null ? data : new User[]{}, groupType));
//    }

    private List<String> getRandomSublist(String[] array, int amount) {
        ArrayList<String> list = new ArrayList<>(amount);
        Random random = new Random();
        while (list.size() < amount) {
            list.add(array[random.nextInt(array.length)]);
        }
        return list;
    }

    public static class SimpleRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private List<Parcelable> mValues;
        private String viewType;

        public static class ViewHolder<V extends View> extends RecyclerView.ViewHolder {
            public Parcelable mBoundObject;

            public final V mView;
            public final ImageView mImageView;
            public final TextView mTextView;

            public ViewHolder(V view) {
                super(view);
                mView = view;
                mImageView = (ImageView) view.findViewById(R.id.avatar);
                mTextView = (TextView) view.findViewById(android.R.id.text1);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTextView.getText();
            }
        }

        public Parcelable getValueAt(int position) {
            return mValues.get(position);
        }

        public SimpleRecyclerViewAdapter(Context context) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
        }

        public void setData(Parcelable[] items, String viewType) {
            mValues = Arrays.asList(items);
            this.viewType = viewType;
        }

        public SimpleRecyclerViewAdapter(Context context, Parcelable[] items) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
            mValues = Arrays.asList(items);
        }

        public SimpleRecyclerViewAdapter(Context context, Parcelable[] items, String viewType) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
            mValues = Arrays.asList(items);
            this.viewType = viewType;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            // Item object
            holder.mBoundObject = mValues.get(position);

            // Text
            holder.mTextView.setText(mValues.get(position).toString());

            // When an item in the list is clicked
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();

                    switch (viewType) {
                        case Constants.STUDENT_GROUP_SCHOOL:
                        case Constants.STUDENT_GROUP_CAMPUS: {
                            Intent intent = new Intent(context, PeopleActivity_.class);

                            Group group = (Group) holder.mBoundObject;
                            group.setType(viewType);

                            PeopleActivity.GroupWrapper wrapper = new PeopleActivity.GroupWrapper(group.getType(), group.getName());

                            intent.putExtra(Constants.GROUP, wrapper);

                            context.startActivity(intent);
                            break;
                        }

                        case Constants.PEOPLE: {
                            Intent intent = new Intent(context, PeopleDetailsActivity.class);

                            intent.putExtra(Constants.USER, holder.mBoundObject);

                            context.startActivity(intent);
                            break;
                        }
                    }
                }
            });

            // Image

            if (!viewType.equals(Constants.PEOPLE)) {
                holder.mImageView.setImageResource(R.drawable.ic_group_black_48dp);
            } else {
                holder.mImageView.setImageResource(R.drawable.ic_person_black_48dp);
            }

//            Glide.with(holder.mImageView.getContext())
//                    .load(!viewType.equals(Constants.PEOPLE)
//                            ? R.drawable.ic_group_black_48dp : R.drawable.ic_person_black_48dp)
//                    .fitCenter()
//                    .into(holder.mImageView);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }
    }

    public class RecyclerViewAdapter
            extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements SectionIndexer, Filterable {

        private DataFilter mFilter;
        private List<Parcelable> mOriginalValues;

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;

        public class ViewHolder<V extends View> extends RecyclerView.ViewHolder {
            public Parcelable mBoundObject;

            public final V mView;
            public final ImageView mImageView;
            public final TextView mTextView;

            public ViewHolder(V view) {
                super(view);
                mView = view;
                mImageView = (ImageView) view.findViewById(R.id.avatar);
                mTextView = (TextView) view.findViewById(android.R.id.text1);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTextView.getText();
            }
        }

        @Override
        public Filter getFilter() {
            if (mFilter == null) {
                mFilter = new DataFilter();
            }
            return mFilter;
        }

        public Parcelable getValueAt(int position) {
            switch (groupType) {
                case Constants.STUDENT_GROUP_SCHOOL:
                case Constants.STUDENT_GROUP_CAMPUS:
                    return groups.get(position);

                case Constants.PEOPLE:
                    return users.get(position);

                default:
                    return null;
            }
        }

        public RecyclerViewAdapter(Context context) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            List<?> values = null;

            switch (groupType) {
                case Constants.STUDENT_GROUP_SCHOOL:
                case Constants.STUDENT_GROUP_CAMPUS:
                    values = groups;
                    // Text
                    holder.mTextView.setText(groups.get(position).toString());
                break;

                case Constants.PEOPLE:
                    values = users;
                    // Text
                    holder.mTextView.setText(users.get(position).toString());
                    break;
            }

            // Item object
            holder.mBoundObject = (Parcelable) values.get(position);



            // When an item in the list is clicked
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();

                    switch (groupType) {
                        case Constants.STUDENT_GROUP_SCHOOL:
                        case Constants.STUDENT_GROUP_CAMPUS: {
                            Intent intent = new Intent(context, PeopleActivity_.class);

                            Group group = (Group) holder.mBoundObject;
                            group.setType(groupType);

                            PeopleActivity.GroupWrapper wrapper = new PeopleActivity.GroupWrapper(group.getType(), group.getName(), group.getAddress());

                            intent.putExtra(Constants.GROUP, wrapper);

                            context.startActivity(intent);
                            break;
                        }

                        case Constants.PEOPLE: {
                            Intent intent = new Intent(context, PeopleDetailsActivity.class);

                            User u = (User) holder.mBoundObject;

                            PeopleDetailsActivity.UserWrapper wrapper = new PeopleDetailsActivity.UserWrapper(u.getName(), u.getEmail());

                            intent.putExtra(Constants.USER, wrapper);

                            context.startActivity(intent);
                            break;
                        }
                    }
                }
            });

            // Image
            Glide.with(holder.mImageView.getContext())
                    .load(!groupType.equals(Constants.PEOPLE)
                            ? R.drawable.ic_group_black_48dp : R.drawable.ic_person_black_48dp)
                    .fitCenter()
                    .into(holder.mImageView);
        }

        @Override
        public int getItemCount() {
            switch (groupType) {
                case Constants.STUDENT_GROUP_SCHOOL:
                case Constants.STUDENT_GROUP_CAMPUS:
                    return groups.size();

                case Constants.PEOPLE:
                    return users.size();

                default: return 0;
            }
        }

        @Override
        public Object[] getSections() {
            //Log.d(TAG, "sectionGroups: " + String.valueOf(sectionGroups.size()));
            return sectionGroups.toArray(new PeopleGroupSectionTitleIndicator.SectionWrapper[sectionGroups.size()]);
        }

        @Override
        public int getPositionForSection(int sectionIndex) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            //Log.d(TAG, String.valueOf(position) + "sectionGroups size: " + sectionGroups.size());

            if (sectionGroups.isEmpty()) {
                Log.d(TAG, "Abandon ship! No section groups to show position for!");
                return 0;
            }

            switch (groupType) {
                case Constants.STUDENT_GROUP_SCHOOL:
                case Constants.STUDENT_GROUP_CAMPUS:
                    if (position >= groups.size()) {
                        position = groups.size() - 1;
                    } else if (position < 0) {
                        position = 0;
                    }
                    break;
                case Constants.PEOPLE:
                    if (position >= users.size()) {
                        position = users.size() - 1;
                    } else if (position < 0) {
                        position = 0;
                    }
                    break;
            }

            switch (groupType) {
                case Constants.STUDENT_GROUP_SCHOOL:
                case Constants.STUDENT_GROUP_CAMPUS:
                    return sectionGroups.indexOf(new PeopleGroupSectionTitleIndicator.SectionWrapper(groupType, groups.get(position).getName().substring(0, 1)));

                case Constants.PEOPLE:
                    User user = users.get(position);

                    switch (selectedGroup.getType()) {
                        // First letter of the group represents the section
                        case Constants.STUDENT_GROUP_SCHOOL:
                            return sectionGroups.indexOf(new PeopleGroupSectionTitleIndicator.SectionWrapper(groupType, selectedGroup.getType(), user.getName().substring(0, 1)));
                        // The room number represents the section
                        case Constants.STUDENT_GROUP_CAMPUS:
                            if (user.getRole().equals(Role.STUDENT.name())) {
                                Student student = (Student) user;
                                return sectionGroups.indexOf(new PeopleGroupSectionTitleIndicator.SectionWrapper(groupType, selectedGroup.getType(), student.getAccomodation().getRoomNumber()));
                            }
                            break;
                    }


                default:
                    return 0;
            }
        }

        private class DataFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();

                if (mOriginalValues == null) {
                    synchronized (mLock) {
                        switch (groupType) {
                            case Constants.STUDENT_GROUP_SCHOOL:
                            case Constants.STUDENT_GROUP_CAMPUS:
                                mOriginalValues = new ArrayList<Parcelable>(groups);
                                break;

                            case Constants.PEOPLE:
                                mOriginalValues = new ArrayList<Parcelable>(users);
                                break;
                        }
                    }
                }

                // If no search query return all the resultset
                if (charSequence == null || charSequence.length() == 0) {
                    ArrayList<Parcelable> list;
                    synchronized (mLock) {
                        list = new ArrayList<Parcelable>(mOriginalValues);
                    }
                    results.values = list;
                    results.count = list.size();
                } else {
                    String prefixString = charSequence.toString().toLowerCase();

                    ArrayList<Parcelable> oldValues = new ArrayList<Parcelable>(mOriginalValues);

                    final int count = oldValues.size();
                    final ArrayList<Parcelable> newValues = new ArrayList<Parcelable>();

                    for (int i = 0; i < count; i++) {
                        final Parcelable value = oldValues.get(i);

                        String valueText = "";

                        switch (groupType) {
                            case Constants.STUDENT_GROUP_SCHOOL:
                            case Constants.STUDENT_GROUP_CAMPUS:
                                valueText = ((Group) value).getName().toLowerCase();
                                break;

                            case Constants.PEOPLE:
                                valueText = ((User) value).getName().toLowerCase();
                                break;
                        }

                        if (valueText.contains(prefixString.toLowerCase())) {
                            newValues.add(value);
                        } else {
                            final String[] words = valueText.split(" ");
                            for (String word : words) {
                                if (word.toLowerCase().contains(
                                        prefixString.toLowerCase())) {
                                    newValues.add(value);
                                    break;
                                }
                            }
                        }
                    }

                    results.values = newValues;
                    results.count = newValues.size();
                }

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charSequence,
                                          FilterResults filterResults) {

                switch (groupType) {
                    case Constants.STUDENT_GROUP_SCHOOL:
                    case Constants.STUDENT_GROUP_CAMPUS:
                        groups = new ArrayList<Group>((Collection<? extends Group>) filterResults.values);
                        break;

                    case Constants.PEOPLE:
                        users = new ArrayList<User>((Collection<? extends User>) filterResults.values);
                        break;
                }

                if (filterResults.count > 0) {
                    computeSections();
                    notifyDataSetChanged();
                }
            }
        }
    }
}
