package ro.unitbv.studnet.brasovstudnet.ui;

import android.graphics.RectF;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.orhanobut.dialogplus.DialogPlus;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.Event;
import ro.unitbv.studnet.brasovstudnet.service.RestClientProxy;

@EActivity(R.layout.activity_schedule)
public class ScheduleActivity extends BaseActivity implements WeekView.MonthChangeListener,
        WeekView.EventClickListener,
        WeekView.EventLongPressListener {

    private static final String TAG = "ScheduleActivity";
    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_THREE_DAY_VIEW = 2;
    private static final int TYPE_WEEK_VIEW = 3;
    private int mWeekViewType = TYPE_THREE_DAY_VIEW;

    @ViewById(R.id.weekView)
    WeekView mWeekView;

    @App
    BrasovStudNetApp app;

    @Bean
    RestClientProxy restClient;

    AtomicInteger eventId = new AtomicInteger();

    List<Event> events = new ArrayList<>();
    List < WeekViewEvent > weekViewEvents = new ArrayList<>();
//    int newYear;
//    int newMonth;
    class CalDate implements Comparable<CalDate> {
        int year;
        int month;

        public CalDate(int newYear, int newMonth) {
            this.year = newYear;
            this.month = newMonth;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CalDate calDate = (CalDate) o;

            if (year != calDate.year) return false;
            return month == calDate.month;

        }

        @Override
        public int hashCode() {
            int result = year;
            result = 31 * result + month;
            return result;
        }

    @Override
    public int compareTo(CalDate another) {
        int i = Integer.valueOf(year).compareTo(another.year);
        if (i != 0)
            return i;

        return Integer.valueOf(month).compareTo(another.month);
    }
}

    Map<CalDate, Boolean> alreadyRequestedData = new HashMap<>();

    @AfterViews
    void afterViews () {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set an action when any event is clicked.
        mWeekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);

        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(false);

        Calendar now = Calendar.getInstance();
        getEvents(now.get(Calendar.YEAR), now.get(Calendar.MONTH));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.schedule_menu, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        setupDateTimeInterpreter(id == R.id.action_week_view);
        switch (id){
            case R.id.action_today:
                mWeekView.goToToday();
                return true;
            case R.id.action_day_view:
                if (mWeekViewType != TYPE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(1);

                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_three_day_view:
                if (mWeekViewType != TYPE_THREE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_THREE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(3);

                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_week_view:
                if (mWeekViewType != TYPE_WEEK_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_WEEK_VIEW;
                    mWeekView.setNumberOfVisibleDays(7);

                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                }
                return true;
        }

        return true;
        //return super.onOptionsItemSelected(item);
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     * @param shortDate True if the date values should be short.
     */
    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());

                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
            }
        });
    }

    @Background
    void getEvents(int year, int month) {
        List<Event> events = restClient.getInstance().getEventsAt(year, month);
        Log.d(TAG, "[" + year + "]" + "[ " + month + "]" + " Events count: " + events.size());
        addEvents(year, month, events);
    }

    @UiThread
    void addEvents(final int year, final int month, final List<Event> events) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alreadyRequestedData.put(new CalDate(year, month), true);

                Random r = new Random(5);

                for (Event event : events) {
                    Calendar startTime = Calendar.getInstance();
                    startTime.setTimeInMillis(event.getStart_at().getTime());

                    Calendar endTime = Calendar.getInstance();
                    endTime.setTimeInMillis(event.getEnd_at().getTime());

                    WeekViewEvent weekViewEvent = new WeekViewEvent(eventId.incrementAndGet(), event.getName(), startTime, endTime);

                    switch (r.nextInt(5)) {
                        case 0:
                        case 1:
                            weekViewEvent.setColor(getResources().getColor(R.color.event_color_01));
                            break;
                        case 2:
                            weekViewEvent.setColor(getResources().getColor(R.color.event_color_02));
                            break;
                        case 3:
                            weekViewEvent.setColor(getResources().getColor(R.color.event_color_03));
                            break;
                        case 4:
                            weekViewEvent.setColor(getResources().getColor(R.color.event_color_04));
                            break;

                    }

                    weekViewEvents.add(weekViewEvent);
                }

                mWeekView.notifyDatasetChanged();
            }
        });


    }

    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {

        Boolean result = alreadyRequestedData.get(new CalDate(newYear, newMonth));

        if (result == null || !result) {
            Log.d(TAG, newYear + " " + newMonth + " | " + newYear + " " + newMonth);
            getEvents(newYear, newMonth);
        }

        return weekViewEvents;

    }

//        Calendar startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        Calendar endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR, 1);
//        endTime.set(Calendar.MONTH, newMonth-1);
//        WeekViewEvent event = new WeekViewEvent(1, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_01));
//        events.add(event);

//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 30);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.set(Calendar.HOUR_OF_DAY, 4);
//        endTime.set(Calendar.MINUTE, 30);
//        endTime.set(Calendar.MONTH, newMonth-1);
//        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_02));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 4);
//        startTime.set(Calendar.MINUTE, 20);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.set(Calendar.HOUR_OF_DAY, 5);
//        endTime.set(Calendar.MINUTE, 0);
//        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_03));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 5);
//        startTime.set(Calendar.MINUTE, 30);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 2);
//        endTime.set(Calendar.MONTH, newMonth-1);
//        event = new WeekViewEvent(2, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_02));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 5);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        startTime.add(Calendar.DATE, 1);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        endTime.set(Calendar.MONTH, newMonth - 1);
//        event = new WeekViewEvent(3, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_03));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.DAY_OF_MONTH, 15);
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        event = new WeekViewEvent(4, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_04));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.DAY_OF_MONTH, 1);
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        event = new WeekViewEvent(5, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_01));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.DAY_OF_MONTH, startTime.getActualMaximum(Calendar.DAY_OF_MONTH));
//        startTime.set(Calendar.HOUR_OF_DAY, 15);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        event = new WeekViewEvent(5, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.event_color_02));
//        events.add(event);

//        return weekViewEvents;
//    }
    private static final Random RANDOM = new Random();



    private String getEventTitle(Calendar time) {
        String[] arr = new String[]{
                "Programare Java pe componente",
                "Programare logica",
                "Programare functionala - Haskell",
                "Programarea dispozitivelor mobile"
            };


              return String.format("%s - %02d:%02d %s/%d", arr[RANDOM.nextInt(arr.length)], time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(ScheduleActivity.this, "Clicked " + event.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(ScheduleActivity.this, "Long pressed event: " + event.getName(), Toast.LENGTH_SHORT).show();
    }

    void showEventInfo(Event event) {
        View eventContent = getLayoutInflater().inflate(R.layout.event_details, null);

        // name
        TextView nameText = (TextView) eventContent.findViewById(R.id.name);
        nameText.setText(event.getName().trim());

        // start
        TextView startAt = (TextView) eventContent.findViewById(R.id.startAt);
        Calendar startTime = Calendar.getInstance();
        startTime.setTimeInMillis(event.getStart_at().getTime());
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' h:mm a");
        startAt.setText(format.format(startTime.getTime()));

        // end
        TextView endAt = (TextView) eventContent.findViewById(R.id.endAt);
        Calendar endTime = Calendar.getInstance();
        endTime.setTimeInMillis(event.getEnd_at().getTime());
        endAt.setText(format.format(startTime.getTime()));

        // description
        TextView description = (TextView) eventContent.findViewById(R.id.description);
        description.setText(event.getDescription().trim());
        description.setMovementMethod(new ScrollingMovementMethod());

        DialogPlus dialog = new DialogPlus.Builder(this)
                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(eventContent))
                .setCancelable(true)
                .setGravity(Gravity.CENTER)
                .create();

        dialog.show();
    }
}
