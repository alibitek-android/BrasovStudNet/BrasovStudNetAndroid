package ro.unitbv.studnet.brasovstudnet.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SystemService;

import java.io.IOException;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.ui.BroadcastMessagingActivity;
import ro.unitbv.studnet.brasovstudnet.ui.ChatActivity;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EBean
public class GcmManager {

    private static final String TAG = "GCMManager";

    private String GCM_SENDER_ID;
    private GoogleCloudMessaging gcm;

    @RootContext
    ChatActivity chatActivity;

    @RootContext
    IntentService service;

    @RootContext
    BroadcastMessagingActivity broadcastMessagingActivity;

    Context context;

    @SystemService
    NotificationManager notificationManager;

    @App
    BrasovStudNetApp app;

    @AfterInject
    void afterInject() {

        if (service != null) {
            context = service;
        } else if (chatActivity != null) {
            context = chatActivity;
        } else if (broadcastMessagingActivity != null) {
            context = broadcastMessagingActivity;
        }

        gcm = GoogleCloudMessaging.getInstance(context);
        GCM_SENDER_ID = context.getString(R.string.gcm_defaultSenderId);
    }

    public GcmManager() {
    }

    public void cleanup() {
        close();
    }

    public boolean sendMessage(String message, String toAccount) {
        try {
            Bundle data = new Bundle();

            String from = app.getAccount();

            // Action (used to distinguish different message types on the server)
            // FIXME: add a messagetype enum parameter to this function
            if (toAccount.contains("@")) {
                data.putString(Constants.ACTION, Constants.ACTION_CHAT_MESSAGE);
            } else {
                data.putString(Constants.ACTION, Constants.ACTION_BROADCAST_MESSAGE);
                data.putString(Constants.MESSAGE_CATEGORY,((BroadcastMessagingActivity) context).getBroadcastMessageCategory());
            }

            // Message
            data.putString("message", message);

            // From account
            data.putString("fromAcc", from);

            // To account
            data.putString("toAcc", toAccount);

            String messageId = Integer.toString(getNextMsgId());

            Log.d(TAG, String.valueOf(data));

            gcm.send(GCM_SENDER_ID + "@gcm.googleapis.com", messageId, Constants.GCM_DEFAULT_TTL, data);

            Log.v(TAG, "Sending message: " + message + " to " + toAccount + " from: " + from);

        } catch (IOException e) {
            Log.e(TAG, "IOException while sending upstream message", e);
            return false;
        }

        return true;
    }

    /**
     * Register the application for GCM and return the registration ID
     * You must call this once, when your application is installed,
     * and send the returned registration ID to the server.
     * @throws IOException
     */
    public String requestToken() throws IOException {
        // Initially this call goes out to the network to retrieve the token, subsequent calls are local.
        InstanceID instanceID = InstanceID.getInstance(context);
        String token = instanceID.getToken(GCM_SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
        setRegistrationId(token);
        return token;
    }
    
    public void unregister() {
        InstanceID instanceID = InstanceID.getInstance(context);
        try {
            instanceID.deleteToken(GCM_SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        removeRegistrationId();
    }

    private String getRegistrationId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return prefs.getString(Constants.KEY_REG_ID, null);
    }

    private void setRegistrationId(String regId) {
        final SharedPreferences prefs = getPrefs();
        //Log.i(TAG, "Saving regId to prefs: " + regId);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.KEY_REG_ID, regId);
        editor.putInt(Constants.KEY_STATE, Constants.State.REGISTERED.ordinal());
        editor.apply();
    }

    private void removeRegistrationId() {
        final SharedPreferences prefs = getPrefs();
        //Log.i(TAG, "Removing regId from prefs");
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(Constants.KEY_REG_ID);
        editor.putInt(Constants.KEY_STATE, Constants.State.UNREGISTERED.ordinal());
        editor.apply();
    }

    private int getNextMsgId() {
        SharedPreferences prefs = getPrefs();
        int id = prefs.getInt(Constants.KEY_MSG_ID, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Constants.KEY_MSG_ID, ++id);
        editor.apply();
        return id;
    }

    private SharedPreferences getPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private void broadcastStatus(boolean status) {
        Intent intent = new Intent(Constants.ACTION_REGISTER);
        intent.putExtra(Constants.EXTRA_STATUS, status ? Constants.STATUS_SUCCESS : Constants.STATUS_FAILED);
        context.sendBroadcast(intent);
    }

    /**
     * Return the message type from an intent passed into a client app's broadcast receiver
     * There are two general categories of messages passed from the server:
     * - regular GCM messages
     * - special GCM status messages.
     * The possible types are:
     * MESSAGE_TYPE_MESSAGE — regular message from your server.
     * MESSAGE_TYPE_DELETED — special status message indicating that some messages have been discarded because they exceeded the storage limits.
     * MESSAGE_TYPE_SEND_ERROR — special status message indicating that there were errors sending one of the messages.
     *
     * You can use this method to filter based on message type.
     * Since it is likely that GCM will be extended in the future with new message types,
     * just ignore any message types you're not interested in, or that you don't recognize.
     * @param intent a GCM intent received from {@link GcmRegistrationIntentService#onHandleIntent(Intent)}
     * @return The message type or null if the intent is not a GCM intent
     */
    private String getMessageType(Intent intent) {
        return gcm.getMessageType(intent);
    }

    /**
     * Must be called when your application is done using GCM, to release internal resources.
     */
    private void close() {
        gcm.close();
    }
}
