package ro.unitbv.studnet.brasovstudnet.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

import java.util.Timer;
import java.util.TimerTask;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.Student;
import ro.unitbv.studnet.brasovstudnet.service.RestClientProxy;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @App
    BrasovStudNetApp app;

    @Bean
    RestClientProxy restClient;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final TimerTask task = new TimerTask() {

            @Override
            public void run() {
                if (app.isLoggedIn()) {
                    Log.d(TAG, "Already logged in!");

//                    final Intent mainIntent = new Intent().setClass(getApplicationContext(), MainActivity_.class);
//                    mainIntent.putExtra(Constants.KEY_ACCOUNT, app.getAccount());
                    Student user = restClient.getInstance().getUserInfo();
//                    Log.d(TAG, user.toString());
                    app.setUser(user);
                    MainActivity_.intent(SplashActivity.this).start();
//                    mainIntent.putExtra(Constants.USER, user);
//                    startActivity(mainIntent);
                }
                else
                {
                    Log.d(TAG, "NOT logged in!");
                    final Intent mainIntent = new Intent().setClass(getApplicationContext(), LoginActivity_.class);
                    startActivity(mainIntent);
                }

                finish();
            }
        };

        final Timer timer = new Timer();
        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean showSplash = getPrefs.getBoolean("isSplashEnabled", true);
        if (showSplash) {
            long splashDelay = 500;
            timer.schedule(task, splashDelay);
        } else {
            final Intent mainIntent = new Intent().setClass(getApplicationContext(), MainActivity_.class);
            startActivity(mainIntent);
        }
    }
}