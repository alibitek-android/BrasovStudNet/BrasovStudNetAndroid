package ro.unitbv.studnet.brasovstudnet.service;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

import ro.unitbv.studnet.brasovstudnet.utils.Constants;

// handle the creation, rotation, and updating of registration tokens.
public class MyGcmInstanceIDListenerService extends InstanceIDListenerService {

    private static final String TAG = "MyInstanceIDLS";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Intent intent = new Intent(Constants.ACTION_TOKEN_REFRESH, null, this, GcmRegistrationIntentService.class);
        startService(intent);
    }
    // [END refresh_token]
}
