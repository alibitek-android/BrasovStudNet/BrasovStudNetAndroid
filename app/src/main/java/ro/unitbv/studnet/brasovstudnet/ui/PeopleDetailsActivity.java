package ro.unitbv.studnet.brasovstudnet.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp_;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.Group;
import ro.unitbv.studnet.brasovstudnet.model.Student;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

public class PeopleDetailsActivity extends AppCompatActivity {

    private static final String TAG = "PeopleDetailsActivity";

    //@ViewById(R.id.top_toolbar)
    Toolbar toolbar;

    //@ViewById(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    //@ViewById(R.id.infoText)
    TextView infoText;

    //@ViewById
    TextView detailsText;

    //@Extra
    UserWrapper user;


    static class UserWrapper implements Parcelable {
        String name;
        String email;

        protected UserWrapper(Parcel in) {
            name = in.readString();
            email = in.readString();
        }

        public static final Creator<UserWrapper> CREATOR = new Creator<UserWrapper>() {
            @Override
            public UserWrapper createFromParcel(Parcel in) {
                return new UserWrapper(in);
            }

            @Override
            public UserWrapper[] newArray(int size) {
                return new UserWrapper[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(email);
        }

        public UserWrapper(String name, String email) {
            this.name = name;
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

//    @AfterExtras
    void initializeViews() {
        Log.d(TAG, "USER is?: " + user);

        toolbar = (Toolbar) findViewById(R.id.top_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        infoText = (TextView) findViewById(R.id.infoText);
        detailsText = (TextView) findViewById(R.id.detailsText);

        loadImage();

        // FloatingActionButton
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Send a message to ", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
//                String loggedInAccount = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Constants.KEY_ACCOUNT, "Not logged in!");
//                intent.putExtra(Constants.CHAT_FROM, loggedInAccount);
//                intent.putExtra(Constants.CHAT_TO, selectedPerson);
//                Log.d(TAG, "Message from: " + loggedInAccount + " TO: " + selectedPerson);
//                startActivity(intent);
                Intent intent = new Intent(PeopleDetailsActivity.this, ChatActivity_.class);
                ChatActivity.UserWrapper ur = new ChatActivity.UserWrapper(user.getName(), user.getEmail());
                intent.putExtra(Constants.USER, ur);
                startActivity(intent);
            }
        });

        collapsingToolbar.setTitle(user.getName());

        infoText.setText("User Name: " + user.getName() + "\n" + "Email: " + user.getEmail() + "\n");

        BrasovStudNetApp_ app = (BrasovStudNetApp_) getApplication();

        Student wholeUser = new Student(user.getName(), user.getEmail());
        for (Group group : app.getGroups()) {
            int position;
            if ((position = group.getUsers().indexOf(wholeUser)) != -1) {
                wholeUser = (Student) group.getUsers().get(position);
                break;
            }
        }

        detailsText.setText(wholeUser.getDetails());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = getIntent().getExtras().getParcelable(Constants.USER);

        setContentView(R.layout.activity_people_details);

        initializeViews();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadImage() {
        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        imageView.setImageResource(R.drawable.communication);
        //Glide.with(this).load(R.drawable.communication).centerCrop().into(imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top, menu);
        return true;
    }
}
