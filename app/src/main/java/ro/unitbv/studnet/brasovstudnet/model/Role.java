package ro.unitbv.studnet.brasovstudnet.model;

import android.os.Parcel;
import android.os.Parcelable;

public enum Role implements Parcelable {
    STUDENT,
    TEACHER,
    SYSADMIN,
    CAMPUS_ADMIN;

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public static final Parcelable.Creator<Role> CREATOR = new Parcelable.Creator<Role>() {

        public Role createFromParcel(Parcel in) {
            return Role.values()[in.readInt()];
        }

        public Role[] newArray(int size) {
            return new Role[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(ordinal());
    }

}
