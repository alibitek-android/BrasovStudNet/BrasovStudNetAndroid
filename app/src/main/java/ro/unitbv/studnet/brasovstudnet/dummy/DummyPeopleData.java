package ro.unitbv.studnet.brasovstudnet.dummy;

import java.util.Arrays;
import java.util.Random;

import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.Group;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

public class DummyPeopleData {

    private static final Random RANDOM = new Random();

    public static int getRandomCheeseDrawable() {
        switch (RANDOM.nextInt(5)) {
            default:
            case 0:
                return R.drawable.communication;
            case 1:
                return R.drawable.communication;
            case 2:
                return R.drawable.communication;
            case 3:
                return R.drawable.communication;
            case 4:
                return R.drawable.communication;
        }
    }

    public static final User[] people = {
            new User("Person 1", "a@b1.c"),
            new User("Person 2", "a@b2.c"),
            new User("Person 3", "a@b3.c"),
            new User("Person 4", "a@b4.c"),
            new User("Person 5", "a@b5.c"),
            new User("Person 6", "a@b6.c"),
            new User("Person 7", "a@b7.c"),
            new User("Person 8", "a@b8.c"),
            new User("Person 9", "a@b9.c"),
            new User("Person 10", "a@b10.c"),
    };

    public static final Group[] schoolGroups = {
            new Group("Semigroup", Constants.STUDENT_GROUP_SCHOOL, Arrays.asList(people)),
            new Group("Group", Constants.STUDENT_GROUP_SCHOOL, Arrays.asList(people)),
            new Group("Class", Constants.STUDENT_GROUP_SCHOOL, Arrays.asList(people)),
            new Group("School", Constants.STUDENT_GROUP_SCHOOL, Arrays.asList(people)),
            new Group("Teachers", Constants.STUDENT_GROUP_SCHOOL, Arrays.asList(people))
    };

    public static final Group[] campusGroups = {
            new Group("Room", Constants.STUDENT_GROUP_CAMPUS, Arrays.asList(people)),
            new Group("Floor Nearby", Constants.STUDENT_GROUP_CAMPUS, Arrays.asList(people)),
            new Group("Floor", Constants.STUDENT_GROUP_CAMPUS, Arrays.asList(people)),
            new Group("Dorm", Constants.STUDENT_GROUP_CAMPUS, Arrays.asList(people)),
            new Group("Campus", Constants.STUDENT_GROUP_CAMPUS, Arrays.asList(people)),
    };


}
