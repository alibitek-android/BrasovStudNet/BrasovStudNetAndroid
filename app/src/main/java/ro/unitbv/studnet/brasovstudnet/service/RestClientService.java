package ro.unitbv.studnet.brasovstudnet.service;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.androidannotations.api.rest.RestClientSupport;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import ro.unitbv.studnet.brasovstudnet.model.Event;
import ro.unitbv.studnet.brasovstudnet.model.Student;
import ro.unitbv.studnet.brasovstudnet.model.Token;
import ro.unitbv.studnet.brasovstudnet.model.Topic;
import ro.unitbv.studnet.brasovstudnet.model.User;

@Rest(rootUrl = "http://192.168.1.242:8000/api/1",
        converters = { MappingJackson2HttpMessageConverter.class },
        interceptors = { HttpBasicAuthenticatorInterceptor.class, RequestHeadersInterceptor.class })
@Accept(MediaType.APPLICATION_JSON)
public interface RestClientService extends RestClientSupport, RestClientErrorHandling {

    @Get("/users/{id}")
    User getUserInfoById(String id);

    @Get("/users/profile")
    Student getUserInfo();

    @Get("/users/students/topic?topicAddress={topicAddress}")
    List<Student> getStudentsForTopic(String topicAddress);

    @Post("/tokens")
    void registerToken(Token token);

    @Post("/topics/announceSubscribed")
    void subscribedToTopics(List<Topic> topics);

    @Get("/events")
    List<Event> getEvents();

    @Get("/events/year/{year}/month/{month}")
    List<Event> getEventsAt(int year, int month);
}