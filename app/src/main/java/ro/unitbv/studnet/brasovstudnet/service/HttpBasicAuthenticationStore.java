package ro.unitbv.studnet.brasovstudnet.service;

import android.content.Context;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;

@EBean(scope = EBean.Scope.Singleton)
public class HttpBasicAuthenticationStore {

    @App
    BrasovStudNetApp app;

    private String account;
    private String password;

    public void setAccount(String account) {
        this.account = account;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccount() {
        return account != null ? account : app.getAccount();
    }

    public String getPassword() {
        return password != null ? password : app.getPassword();
    }
}
