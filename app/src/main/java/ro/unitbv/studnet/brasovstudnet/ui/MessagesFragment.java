package ro.unitbv.studnet.brasovstudnet.ui;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;

public class MessagesFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = "MessagesFragment";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat[] df = new DateFormat[] { DateFormat.getDateInstance(), DateFormat.getTimeInstance()};

    private OnFragmentInteractionListener mListener;
    private CursorAdapter chatCursorAdapter;
    private Date now;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        now = new Date();

        chatCursorAdapter = new ChatCursorAdapter(getActivity(), null);
        setListAdapter(chatCursorAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getListView().setDivider(null);
        getListView().setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        getListView().setStackFromBottom(true);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getListView().setLayoutParams(params);

        Bundle bundle = new Bundle();
        bundle.putString(DataProvider.COL_EMAIL, mListener.getPeerEmail());
        getLoaderManager().initLoader(0, bundle, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        String getPeerEmail();
    }

    private String getDisplayTime(String datetime) {
//        try {
//            Date dt = sdf.parse(datetime);
//            if (now.getYear() == dt.getYear() && now.getMonth()==dt.getMonth() && now.getDate()==dt.getDate()) {
//                return df[1].format(dt);
//            }
//            return df[0].format(dt);
//        } catch (ParseException e) {
//            return datetime;
//        }
        return datetime;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String profileEmail = args.getString(DataProvider.COL_EMAIL);
        return new CursorLoader(getActivity(),
                DataProvider.CONTENT_URI_MESSAGES,
                null,
                DataProvider.COL_SENDER_EMAIL + " = ? or " + DataProvider.COL_RECEIVER_EMAIL + " = ?",
                new String[]{profileEmail, profileEmail},
                DataProvider.COL_TIME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        chatCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        chatCursorAdapter.swapCursor(null);
    }

    public class ChatCursorAdapter extends CursorAdapter {

        public ChatCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
        }

        @Override public int getCount() {
            return getCursor() == null ? 0 : super.getCount();
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int _position) {
            Cursor cursor = (Cursor) getItem(_position);
            return getItemViewType(cursor);
        }

        private int getItemViewType(Cursor _cursor) {
            int typeIdx = _cursor.getColumnIndex(DataProvider.COL_TYPE);
            int type = _cursor.getInt(typeIdx);
            return type == 0 ? 0 : 1;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();
            View itemLayout = null;

            switch(getItemViewType(cursor)){
                case 0: // toUser
                    itemLayout = LayoutInflater.from(context).inflate(R.layout.activity_chat_list_item_left_aligned, parent, false);
                    break;
                case 1: // fromUser
                    itemLayout = LayoutInflater.from(context).inflate(R.layout.activity_chat_list_item_right_aligned, parent, false);
                    break;
            }

            if (itemLayout != null) {
                itemLayout.setTag(holder);
                holder.avatar = (ImageView) itemLayout.findViewById(R.id.avatar);
                holder.messageTimestamp = (TextView) itemLayout.findViewById(R.id.messageTimestamp);
                holder.messageText = (TextView) itemLayout.findViewById(R.id.messageText);
            }
            else
            {
                Log.e(TAG, "itemLayout is null!!!");
            }

            return itemLayout;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ViewHolder holder = (ViewHolder) view.getTag();

//            String email = cursor.getString(cursor.getColumnIndex(DataProvider.COL_SENDER_EMAIL));
            holder.messageTimestamp.setText(getDisplayTime(cursor.getString(cursor.getColumnIndex(DataProvider.COL_TIME))));
            holder.messageText.setText(cursor.getString(cursor.getColumnIndex(DataProvider.COL_MESSAGE)));
        }
    }

    private static class ViewHolder {
        TextView messageTimestamp;
        TextView messageText;
        ImageView avatar;
    }
}
