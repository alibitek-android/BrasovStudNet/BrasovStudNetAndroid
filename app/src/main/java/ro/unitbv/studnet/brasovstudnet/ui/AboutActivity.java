package ro.unitbv.studnet.brasovstudnet.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.EActivity;

import ro.unitbv.studnet.brasovstudnet.R;

@EActivity(R.layout.activity_about)
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
