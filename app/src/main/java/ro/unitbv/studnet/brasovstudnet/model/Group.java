package ro.unitbv.studnet.brasovstudnet.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.List;

public class Group implements Comparable<Group>, Parcelable {
    private String name;
    private String type;
    private String address;
    private List<User> users;

    public Group() {

    }

    public Group(String name, String type, String address) {
        this.name = name;
        this.type = type;
        this.address = address;
    }

    public Group(String name, String type, String address, List<User> users) {
        this.name = name;
        this.type = type;
        this.address = address;
        this.users = users;
    }

    public Group(String name, String type, String address, User[] users) {
        this.name = name;
        this.type = type;
        this.address = address;
        this.users = Arrays.asList(users);
    }

    public Group(String name, String type, User[] users) {
        this.name = name;
        this.type = type;
        this.users = Arrays.asList(users);
    }

    public Group(String name, String type, List<User> users) {
        this.name = name;
        this.type = type;
        this.users = users;
    }

    private Group(Parcel in) {
        name = in.readString();
        type = in.readString();
        address = in.readString();
        users = in.createTypedArrayList(User.CREATOR);
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(address);
        dest.writeTypedList(users);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (!name.equals(group.name)) return false;
        if (!type.equals(group.type)) return false;
        return users.equals(group.users);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + users.hashCode();
        return result;
    }


    @Override
    public int compareTo(@NonNull Group another) {
        int res = name.compareTo(another.getName());

        if (res != 0)
            return res;

        return name.compareTo(another.getType());
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return name;
    }
}
