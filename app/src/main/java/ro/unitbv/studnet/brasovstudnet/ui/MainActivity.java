package ro.unitbv.studnet.brasovstudnet.ui;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp;
import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp_;
import ro.unitbv.studnet.brasovstudnet.R;
import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.model.Role;
import ro.unitbv.studnet.brasovstudnet.model.Topic;
import ro.unitbv.studnet.brasovstudnet.model.TopicType;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.service.GcmRegistrationIntentService;
import ro.unitbv.studnet.brasovstudnet.service.GcmRegistrationIntentService_;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

//    private String account;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            // WIFI only
//            if (netInfo.getType() != ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(MainActivity.this, "This app doesn't work without WiFi", Toast.LENGTH_LONG).show();
//                return false;
//            }

            return true;
        }

        return false;
    }

    @ViewById(R.id.viewpager)
    ViewPager viewPager;

    @AfterViews
    void initializeViews() {

        // Toolbar
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_48dp);
        toolbar.setTitle(getString(R.string.app_name));
//        setSupportActionBar(toolbar);
        //toolbar.inflateMenu(R.menu.menu_top);

//        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_48dp);
//        ab.setDisplayHomeAsUpEnabled(true);

        // NavigationView
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        if (navigationView != null) {
//            setupDrawerContent(navigationView);
//        }


//        if (account == null)
//            account = app.getAccount();
//
//        if (account != null) {
//            String[] user = account.split("@");
//            if (user.length == 2)
//            {
//                String lastName = user[0].substring(0, 1).toUpperCase() + user[0].substring(1);
//                String firstName = user[1].substring(0, 1).toUpperCase() + user[1].substring(1);
//                headerUsername.setText(firstName + " " + lastName);
//            } else {
//                headerUsername.setVisibility(View.GONE);
//            }
//
//            headerEmail.setText(account);
//        }

        // ViewPager
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        // FloatingActionButton
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (viewPager != null) {
                    Intent intent = new Intent(getApplicationContext(), BroadcastMessagingActivity_.class);
                    String from = app.getAccount();
                    String to = viewPager.getAdapter().getPageTitle(viewPager.getCurrentItem()).toString();

                    intent.putExtra(Constants.CHAT_FROM, from);
                    intent.putExtra(Constants.CHAT_TO, to);

//                    Log.d(TAG, "Message from: " + app.getAccount() + " TO: " + to);
                    startActivity(intent);
                }
                else {
                    Snackbar.make(view, "ViewPager is null, can't open activity to send message", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Intent intent = getIntent();

//        if (intent.hasExtra(Constants.KEY_MESSAGE_TXT)) {
//            String message = intent.getStringExtra(Constants.KEY_MESSAGE_TXT);
//            Log.d(TAG, message);
//        }

//        account = intent.getStringExtra(Constants.KEY_ACCOUNT);
//        if (account == null) {
//            account = app.getAccount();
//            if (account == null) {
//                Intent loginIntent = new Intent(getApplicationContext(), LoginActivity_.class);
//                startActivity(loginIntent);
//                finish();
//            }
//        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();

                switch (action) {
                    case BrasovStudNetApp_.REGISTRATION_COMPLETE: {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                        boolean sentToken = sharedPreferences.getBoolean(BrasovStudNetApp_.SENT_TOKEN_TO_SERVER, false);

                        if (sentToken) {
                            Toast.makeText(context, getString(R.string.gcm_send_message), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, getString(R.string.token_error_message), Toast.LENGTH_LONG).show();
                        }

                        break;
                    }
                }

            }
        };

        //mPeopleBtn = (Button) findViewById(R.id.peopleBtn);

        if (checkPlayServices()) {
            // Start IntentService to requestToken this application with GCM.
            registerDevice();
        }
    }

    ListFragment campusGroup;
    ListFragment schoolGroup;

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());

        if (app.getUser() != null) {
            Log.d(TAG, app.getUser().getTopics().get(0).toString());

            campusGroup = new ListFragment_();

            Bundle campusBundle = new Bundle();
            campusBundle.putString(Constants.GROUP_TYPE, Constants.STUDENT_GROUP_CAMPUS);

            campusGroup.setArguments(campusBundle);

            adapter.addFragment(campusGroup, getString(R.string.CampusGroup));

            schoolGroup = new ListFragment_();
            Bundle schoolBundle = new Bundle();
            schoolBundle.putString(Constants.GROUP_TYPE, Constants.STUDENT_GROUP_SCHOOL);
//        List<Topic> topicsByType2 = app.getUser().getTopicsByType(TopicType.SCHOOL);
//        schoolBundle.putParcelableArray(Constants.TOPICS, (Topic[]) topicsByType2.toArray(new Topic[topicsByType2.size()]));
            schoolGroup.setArguments(schoolBundle);

            adapter.addFragment(schoolGroup, getString(R.string.SchoolGroup));

//        ListFragment favoriteGroup = new ListFragment();
//        Bundle favoriteBundle = new Bundle();
//        favoriteBundle.putString(Constants.GROUP_TYPE, Constants.FAVORITES_GROUP);
//        favoriteGroup.setArguments(favoriteBundle);
//        adapter.addFragment(favoriteGroup, getString(R.string.FavoritesGroup));
        } else {
            campusGroup = new ListFragment_();
            adapter.addFragment(campusGroup, getString(R.string.CampusGroup));
            schoolGroup = new ListFragment_();
            adapter.addFragment(schoolGroup, getString(R.string.SchoolGroup));
        }

        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    private void registerDevice() {
        Intent regIntent = new Intent(this, GcmRegistrationIntentService_.class);
        regIntent.putExtra(Constants.KEY_ACCOUNT, app.getAccount());
        regIntent.setAction(Constants.ACTION_REGISTER);
        startService(regIntent);
    }

    private void unregisterDevice() {
        Intent regIntent = new Intent(this, GcmRegistrationIntentService_.class);
        regIntent.setAction(Constants.ACTION_UNREGISTER);
        startService(regIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == LOGIN_REQUEST) {
//            if (resultCode == RESULT_OK) {
//                NavUtils.navigateUpTo(this, new Intent(MainActivity.this, PeopleListActivity.class));
//            }
//        }
        //NavUtils.navigateUpTo(this, new Intent(MainActivity.this, LoginActivity.class));
    }

    public void peopleBtnClicked(View view) {
        //startActivityForResult(new Intent(MainActivity.this, PeopleListActivity.class), PEOPLE_REQUEST);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(BrasovStudNetApp_.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

//    private void initToolbars() {
//
//
////        Toolbar toolbarBottom = (Toolbar) findViewById(R.id.toolbar_bottom);
////        buttomToolbarMenu = (ActionMenuView) toolbarBottom.findViewById(R.id.buttom_toolbar_menu);
////        buttomToolbarMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
////            @Override
////            public boolean onMenuItemClick(MenuItem item) {
////                int id = item.getItemId();
////
////                switch(id) {
////                    case R.id.action_people:
////                        startActivity(new Intent(MainActivity.this, PeopleListActivity.class));
////                        break;
////
////                    case R.id.action_messages:
////                        break;
////
////                    case R.id.actions_notifications:
////                        break;
////
////                    default:
////                        Toast.makeText(MainActivity.this,
////                                "[Bottom Toolbar] You clicked on an unhandled menu item! Menu Item ID: " + String.valueOf(id) + " Menu Title: " + item.getTitle(),
////                                Toast.LENGTH_LONG).show();
////                        break;
////                }
////                return true;
////            }
////        });
//
//        //getMenuInflater().inflate(R.menu.menu_drawer, buttomToolbarMenu.getMenu());
//    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        if (searchView != null) {
            // Associate searchable configuration with the SearchView
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    final String tabTitle = viewPager.getAdapter().getPageTitle(viewPager.getCurrentItem()).toString();
                    Log.i(TAG, "Tab: " + tabTitle);

//                    ArrayAdapter<User> adapter = new ArrayAdapter<User>(PeopleActivity.this, android.R.layout.simple_list_item_1, peopleFragment.users);
//                    adapter.getFilter().filter(s.toLowerCase());
                    if (tabTitle.toUpperCase().equals("CAMPUS")) {
                        campusGroup.viewAdapter.getFilter().filter(s.toLowerCase());
                    } else if (tabTitle.equals("SCHOOL")) {
                        schoolGroup.viewAdapter.getFilter().filter(s.toLowerCase());
                    }

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    final String tabTitle = viewPager.getAdapter().getPageTitle(viewPager.getCurrentItem()).toString();
                    Log.i(TAG, "Tab: " + tabTitle);

//                    ArrayAdapter<User> adapter = new ArrayAdapter<User>(PeopleActivity.this, android.R.layout.simple_list_item_1, peopleFragment.users);
                    if (tabTitle.toUpperCase().equals("CAMPUS")) {
                        campusGroup.viewAdapter.getFilter().filter(s.toLowerCase());
                    } else if (tabTitle.toUpperCase().equals("SCHOOL")) {
                        schoolGroup.viewAdapter.getFilter().filter(s.toLowerCase());
                    }
                    return true;
                }
            });
        } else {
            Log.w(TAG, "searchView is null");
        }

        return true;
    }

    static {
        Thread.setDefaultUncaughtExceptionHandler(
                new Thread.UncaughtExceptionHandler() {
                    public void uncaughtException(Thread aThread, Throwable aThrowable) {
                        Log.wtf("Uncaught exception", aThrowable);
                    }
                });
    }
}
