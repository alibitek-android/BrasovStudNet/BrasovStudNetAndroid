package ro.unitbv.studnet.brasovstudnet;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;
import android.util.Patterns;

import org.androidannotations.annotations.EApplication;

import java.util.ArrayList;
import java.util.List;

import ro.unitbv.studnet.brasovstudnet.model.DataProvider;
import ro.unitbv.studnet.brasovstudnet.model.Group;
import ro.unitbv.studnet.brasovstudnet.model.User;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EApplication
public class BrasovStudNetApp extends MultiDexApplication {
    private static final String TAG = "BrasovStudNetApp";

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String SUBSCRIBED_TO_TOPICS = "subscribedToTopics";

    // Sender constants
    public static final String NAME = "name";
    public static final String SENDER_ID = "senderId";
    public static final String API_KEYS = "apiKeys";
    public static final String TEST_APP_TOKEN = "testAppToken";
    public static final String OTHER_TOKENS = "otherTokens";
    public static final String TOPICS = "topics";
    public static final String GROUPS = "groups";

    public static final String ACTION_REGISTER = "ro.unitbv.studnet.brasovstudnet.REGISTER";
    public static final String EXTRA_STATUS = "status";
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_FAILED = 0;
    public static String[] email_arr;
    private static SharedPreferences prefs;

    private User user;
    private List<Group> groups = new ArrayList<>();

    public List<Group> getGroups() {
        return groups;
    }

    public void addGroups(List<Group> groups) {
        //clearGroups();
        this.groups.addAll(groups);
    }

    public void clearGroups() {
        this.groups.clear();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        List<String> emailList = getEmailList();
        email_arr = emailList.toArray(new String[emailList.size()]);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private List<String> getEmailList() {
        List<String> lst = new ArrayList<>();
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            if (Patterns.EMAIL_ADDRESS.matcher(account.name).matches()) {
                lst.add(account.name);
            }
        }
        return lst;
    }

    public static String getPreferredEmail() {
        return prefs.getString("chat_email_id", email_arr.length==0 ? "" : email_arr[0]);
    }

    public static String getDisplayName() {
        String email = getPreferredEmail();
        return prefs.getString("display_name", email.substring(0, email.indexOf('@')));
    }

    public static boolean isNotify() {
        return prefs.getBoolean("notifications_new_message", true);
    }

    public static String getRingtone() {
        return prefs.getString("notifications_new_message_ringtone", android.provider.Settings.System.DEFAULT_NOTIFICATION_URI.toString());
    }

    public void setAccount(String account) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.KEY_ACCOUNT, account);
        editor.apply();
    }

    public void setPassword(String password) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.KEY_PASSWORD, password);
        editor.apply();
    }

    public String getAccount() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return prefs.getString(Constants.KEY_ACCOUNT, null);
    }

    public String getPassword() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return prefs.getString(Constants.KEY_PASSWORD, null);
    }

    public boolean isLoggedIn() {
        String account = getAccount();
        return account != null && !account.isEmpty();
    }

    public void setLoggedIn(boolean toggle) {
        if (!toggle) {
            setAccount(null);
            setPassword(null);
        } else {
            setAccount("yes");
            setPassword("yes");
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getMessageCategoryIcon(String category) {
        int resId = R.drawable.ic_school_black_48dp;

        if (category == null) {
            return resId;
        } else if (category.equals(DataProvider.BroadcastMessageCategory.Announcements.name())) {
            resId = R.drawable.ic_announcement_black_48dp;
        } else if (category.equals(DataProvider.BroadcastMessageCategory.Events.name())) {
            resId = R.drawable.ic_event_black_48dp;
        } else if (category.equals(DataProvider.BroadcastMessageCategory.Assessments.name())) {
            resId = R.drawable.ic_assessment_black_48dp;
        } else if (category.equals(DataProvider.BroadcastMessageCategory.Assignments.name())) {
            resId = R.drawable.ic_assignment_black_48dp;
        } else if (category.equals(DataProvider.BroadcastMessageCategory.Business.name())) {
            resId = R.drawable.ic_business_black_48dp;
        } else if (category.equals(DataProvider.BroadcastMessageCategory.Polls.name())) {
            resId = R.drawable.ic_poll_black_48dp;
        }

        return resId;
    }
}