package ro.unitbv.studnet.brasovstudnet.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;

import java.io.IOException;

import ro.unitbv.studnet.brasovstudnet.BrasovStudNetApp_;
import ro.unitbv.studnet.brasovstudnet.model.Token;
import ro.unitbv.studnet.brasovstudnet.model.Topic;
import ro.unitbv.studnet.brasovstudnet.utils.Constants;

@EIntentService
public class GcmRegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    @Bean
    RestClientProxy restClient;

    @Bean
    GcmManager gcmManager;

    public GcmRegistrationIntentService() {
        super(TAG);
    }

    String token;

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {

            // In the (unlikely) event that multiple refresh operations occur simultaneously,
            // ensure that they are processed sequentially.
            synchronized (TAG) {

//                String messageType = gcmManager.getMessageType(intent);
//                Log.d("Message Type: ", messageType);

//                if (messageType.equals(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE)) {
                    String action = intent.getAction();

                    switch (action) {
                        case Constants.ACTION_REGISTER: {
                            token = gcmManager.requestToken();

                            Log.d(TAG, "[ACTION_REGISTER] Token: " + token);

                            if (!sharedPreferences.getBoolean(BrasovStudNetApp_.SENT_TOKEN_TO_SERVER, false)) {

                                sendRegistrationToServer(token);
                            } else {
                                Log.d(TAG, "Token already sent to server");
                            }

                            subscribeTopics(token);

                            sharedPreferences.edit().putBoolean(BrasovStudNetApp_.SENT_TOKEN_TO_SERVER, true).apply();
                        }
                        break;

                        case Constants.ACTION_TOKEN_REFRESH: {
                            String token = gcmManager.requestToken();
                            Log.d(TAG, "[ACTION_TOKEN_REFRESH] Token: " + token);
                            sendRegistrationToServer(token);
                            if (!PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                    .getBoolean(BrasovStudNetApp_.SENT_TOKEN_TO_SERVER, false)) {
                                sharedPreferences.edit().putBoolean(BrasovStudNetApp_.SENT_TOKEN_TO_SERVER, true).apply();
                            }
                            break;
                        }

                        case Constants.ACTION_UNREGISTER:
                            break;
                    }
                }
//            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPreferences.edit().putBoolean(BrasovStudNetApp_.SENT_TOKEN_TO_SERVER, false).apply();
        }

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(BrasovStudNetApp_.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        restClient.getInstance().registerToken(new Token(token));
        // TODO: Add custom implementation, as needed.
        Log.d(TAG, "Sending registration token to the server");
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (!sharedPreferences.getBoolean(BrasovStudNetApp_.SUBSCRIBED_TO_TOPICS, false)) {

            Log.d(TAG, "Subscribing to " + gcmManager.app.getUser().getTopics().size() + " topics");

            GcmPubSub pubSub = GcmPubSub.getInstance(this);

            for (Topic topic : gcmManager.app.getUser().getTopics()) {
                pubSub.subscribe(token, "/topics/" + topic.getAddress(), null);
            }

            sharedPreferences.edit().putBoolean(BrasovStudNetApp_.SUBSCRIBED_TO_TOPICS, true).apply();

            restClient.instance.subscribedToTopics(gcmManager.app.getUser().getTopics());
        } else {
            Log.i(TAG, "Already subscribed to topics");
        }
    }

    // [END subscribe_topics]
}
